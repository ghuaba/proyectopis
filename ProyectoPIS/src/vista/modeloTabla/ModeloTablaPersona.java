/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vista.modeloTabla;

import controlador.ed.lista.ListaEnlazada;
import javax.swing.table.AbstractTableModel;
import modelo.Persona;

/**
 *
 * @author mt3k
 */
public class ModeloTablaPersona extends AbstractTableModel {

    private ListaEnlazada<Persona> lista = new ListaEnlazada<>();

    public ListaEnlazada<Persona> getLista() {
        return lista;
    }

    public void setLista(ListaEnlazada<Persona> lista) {
        this.lista = lista;
    }

    @Override
    public int getColumnCount() {
        return 7;
    }

    @Override
    public int getRowCount() {
        return lista.size();
    }

    @Override
    public Object getValueAt(int i, int i1) {
        Persona s = null;

        try {
            s = lista.get(i);
        } catch (Exception e) {
        }
        switch (i1) {
            case 0:
                return (s != null) ? s.getNombres() : "NO DEFINIDO";
            case 1:
                return (s != null) ? s.getApellidos() : "NO DEFINIDO";
            case 2:
                return (s != null) ? s.getEdad() : "NO DEFINIDO";
            case 3:
                return (s != null) ? s.getCedula() : "NO DEFINIDO";
            case 4:
                return (s != null) ? s.getSexo() : "NO DEFINIDO";
            case 5:
                return (s != null) ? s.getEstado() : "NO DEFINIDO";
            case 6:
                return (s != null) ? s.getCorreo() : "NO DEFINIDO";

            default:
                return null;
        }
    }

    @Override
    public String getColumnName(int column) {

        switch (column) {
            case 0:
                return "NOMBRES";
            case 1:
                return "APELLIDOS";
            case 2:
                return "EDAD";
            case 3:
                return "CEDULA";
            case 4:
                return "SEXO";
            case 5:
                return "ESTADO";
            case 6:
                return "CORREO";
            default:
                return null;
        }
    }

}
