/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vista.modeloTabla;

import controlador.ed.lista.ListaEnlazada;
import javax.swing.table.AbstractTableModel;
import modelo.Dignidad;

/**
 *
 * @author mt3k
 */
public class ModeloTablaDignidad extends AbstractTableModel {

    private ListaEnlazada<Dignidad> lista = new ListaEnlazada<>();

    public ListaEnlazada<Dignidad> getLista() {
        return lista;
    }

    public void setLista(ListaEnlazada<Dignidad> lista) {
        this.lista = lista;
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public int getRowCount() {
        return lista.size();
    }

    @Override
    public Object getValueAt(int i, int i1) {
        Dignidad s = null;

        try {
            s = lista.get(i);
        } catch (Exception e) {
        }
        switch (i1) {
            case 0:
                return (s != null) ? s.getId() : "NO DEFINIDO";
            case 1:
                return (s != null) ? s.getNombre() : "NO DEFINIDO";
            case 2:
                return (s != null) ? s.getNro() : "NO DEFINIDO";

            default:
                return null;
        }
    }

    @Override
    public String getColumnName(int column) {

        switch (column) {
            case 0:
                return "ID";
            case 1:
                return "NOMBRE";
            case 2:
                return "NRO DE CANDIDATOS";

            default:
                return null;
        }
    }

}
