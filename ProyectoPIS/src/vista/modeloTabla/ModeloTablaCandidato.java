/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vista.modeloTabla;

import controlador.dao.DignidadDao;
import controlador.dao.PersonaDAO;
import controlador.ed.lista.ListaEnlazada;
import javax.swing.table.AbstractTableModel;
import modelo.Candidato;

/**
 *
 * @author mt3k
 */
public class ModeloTablaCandidato extends AbstractTableModel {

    private ListaEnlazada<Candidato> lista = new ListaEnlazada<>();

    public ListaEnlazada<Candidato> getLista() {
        return lista;
    }

    public void setLista(ListaEnlazada<Candidato> lista) {
        this.lista = lista;
    }

    @Override
    public int getColumnCount() {
        return 6;
    }

    @Override
    public int getRowCount() {
        return lista.size();
    }

    @Override
    public Object getValueAt(int i, int i1) {
        Candidato s = null;

        try {
            s = lista.get(i);
        } catch (Exception e) {
        }
        switch (i1) {

            case 0:
                return (s != null) ? new PersonaDAO().obtener(s.getId_persona()).getNombres() : "NO DEFINIDO";
            case 1:
                return (s != null) ? new PersonaDAO().obtener(s.getId_persona()).getApellidos() : "NO DEFINIDO";
            case 2:
                return (s != null) ? new PersonaDAO().obtener(s.getId_persona()).getEdad() : "NO DEFINIDO";
            case 3:
                return (s != null) ? new PersonaDAO().obtener(s.getId_persona()).getSexo() : "NO DEFINIDO";
            case 4:
                return (s != null) ? new PersonaDAO().obtener(s.getId_persona()).getCedula() : "NO DEFINIDO";
            case 5:
                return (s != null) ? new DignidadDao().obtener(s.getId_dignidad()).getNombre() : "NO DEFINIDO";

            default:
                return null;
        }
    }

    @Override
    public String getColumnName(int column) {

        switch (column) {
            case 0:
                return "NOMBRE";
            case 1:
                return "APELLIDO";
            case 2:
                return "EDAD ";
            case 3:
                return "GENERO";
            case 4:
                return "CEDULA";
            case 5:
                return "DIGNIDAD";

            default:
                return null;
        }
    }

}
