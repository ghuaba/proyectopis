package vista.modeloTabla;

import controlador.ed.lista.ListaEnlazada;
import javax.swing.table.AbstractTableModel;
import modelo.Voto;

/**
 *
 * @author FA506ICB-HN114W
 */
public class ModeloTablaVotos extends AbstractTableModel {

    @Override
    public int getRowCount() {
        return 5;
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

//    private ListaEnlazada<Voto> lista = new ListaEnlazada<>();
//
//    public ListaEnlazada<Voto> getLista() {
//        return lista;
//    }
//
//    public void setLista(ListaEnlazada<Voto> lista) {
//        this.lista = lista;
//    }
//
//    @Override
//    public int getRowCount() {
//        return lista.size();
//    }
//
//    @Override
//    public int getColumnCount() {
//        return 4;
//    }
//
//    @Override
//    public Object getValueAt(int i, int i1) {
//        Voto v = null;
//        try {
//            v = lista.get(i);
//        } catch (Exception e) {
//        }
//        switch (i1) {
//            case 0:
////                return (v != null) ? v.getDignidad(): "NO DEFINIDO";
//            case 1:
//                return (v != null) ? v.getId_eleccion(): "NO DEFINIDO";
//            case 2: 
//                return (v != null) ? v.getTipo(): "NO DEFINIDO";
//            case 3: 
//                return (v != null) ? v.getVoto(): "NO DEFINIDO";    
//                 default:
//                     return null;
//        }
//    }
//    
//    public String getColumnName(int column) {
//        switch(column){
//            case 0:
//                return "DIGNIDAD";
//            case 1: 
//                return "ID_ELECCION";
//            case 2: 
//                return "TIPO_VOTO";
//            case 3: 
//                return "VOTO";
//            default:
//                return null;
//        }
//    }
}
