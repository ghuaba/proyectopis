/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vista.modeloTabla;

import controlador.ed.lista.ListaEnlazada;
import javax.swing.table.AbstractTableModel;
import modelo.Partido;

/**
 *
 * @author mt3k
 */
public class ModeloTablaPartidoPolitico extends AbstractTableModel {

    private ListaEnlazada<Partido> lista = new ListaEnlazada<>();

    public ListaEnlazada<Partido> getLista() {
        return lista;
    }

    public void setLista(ListaEnlazada<Partido> lista) {
        this.lista = lista;
    }

    @Override
    public int getColumnCount() {
        return 1;
    }

    @Override
    public int getRowCount() {
        return lista.size();
    }

    @Override
    public Object getValueAt(int i, int i1) {
        Partido s = null;

        try {
            s = lista.get(i);
        } catch (Exception e) {
        }
        switch (i1) {
            case 0:
                return (s != null) ? s.getNombre() : "NO DEFINIDO";
            default:
                return null;
        }
    }

    @Override
    public String getColumnName(int column) {

        switch (column) {
            case 0:
                return "NOMBRE";

            default:
                return null;
        }
    }

}
