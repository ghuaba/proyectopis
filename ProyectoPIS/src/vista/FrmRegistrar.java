package vista;

import controlador.dao.PersonaDAO;
import javax.swing.JOptionPane;

/**
 *
 * @author ghuaba
 */
public class FrmRegistrar extends javax.swing.JDialog {

    PersonaDAO cdao = new PersonaDAO();
    private Integer fila = -1;

    /**
     *
     * Creates new form FrmRegistrar
     */
    public FrmRegistrar() {
    }

    /**
     * Alterna la visibilidad de la contraseña ingresada en los campos de
     * contraseña y repetición de contraseña. Si el checkbox de mostrar
     * contraseña está seleccionado, las contraseñas se mostrarán en texto
     * claro. Si el checkbox de mostrar contraseña no está seleccionado, las
     * contraseñas se mostrarán en forma enmascarada.
     */
    private void mostrarPassword() {
        if (chkMostrar.isSelected()) {                         // Si el checkbox "Mostrar contraseña" está seleccionado
            txtPassword.setEchoChar((char) 0);                  // Mostrar la contraseña en texto claro
            txtRepetirPassword.setEchoChar((char) 0);           // Mostrar la repetición de contraseña en texto claro
        } else {
            txtPassword.setEchoChar('*');                       // Mostrar la contraseña enmascarada (asteriscos)
            txtRepetirPassword.setEchoChar('*');                // Mostrar la repetición de contraseña enmascarada (asteriscos)
        }
    }

    /**
     * Limpia los campos del formulario de registro y restaura valores
     * predeterminados.
     */
    private void limpiar() {
        txtNombre.setText("");                      // Limpiar campo de nombre
        txtApellido.setText("");                    // Limpiar campo de apellido
        cbxSexo.setSelectedIndex(0);                // Restaurar selección predeterminada en el campo de sexo
        jspEdad.setValue(16);                       // Restaurar valor predeterminado en el campo de edad
        txtCorreo.setText("");                      // Limpiar campo de correo
        txtCedula.setText("");                      // Limpiar campo de cédula
        txtPassword.setText("");                    // Limpiar campo de contraseña
        txtRepetirPassword.setText("");             // Limpiar campo de repetición de contraseña

        cdao.setPersona(null);                      // Establecer el objeto de persona en null en el DAO
        fila = -1;                                   // Restablecer la fila seleccionada a un valor inicial
    }

    /**
     * Realiza el proceso de registro de una nueva cuenta de votante.
     */
    private void save() {
        if (!txtCedula.getText().trim().isEmpty() && txtCedula.getText().length() == 10
                && !txtApellido.getText().trim().isEmpty()
                && !txtNombre.getText().trim().isEmpty()
                && !txtCorreo.getText().trim().isEmpty()
                && !txtPassword.getText().trim().isEmpty()
                && !jspEdad.getValue().toString().isEmpty()
                //     && !cbxSexo.getAction().toString().isEmpty()
                && txtRepetirPassword.getText().trim().equals(txtPassword.getText())) {

            try {
                // Validar la cédula
                boolean cedulaValida = PersonaDAO.validarCedula(txtCedula.getText());
                if (!cedulaValida) {
                    JOptionPane.showMessageDialog(null, "La cédula es incorrecta", "ERROR", JOptionPane.ERROR_MESSAGE);
                    return; // Salir del método sin realizar el registro
                }

                // Verificar si la cédula ya está registrada
//                boolean cedulaExiste = cdao.cedulaExistentes(txtCedula.getText());
//                if (!cedulaExiste) {
//                    JOptionPane.showMessageDialog(null, "La cedula ya se encuentra registrada", "ERROR", JOptionPane.ERROR_MESSAGE);
//                    return; // Salir del método sin realizar el registro
//                }
                /**
                 *
                 * @annotation verifica la existencia de correos ya reguistrado
                 */
                boolean correoExiste = cdao.correoRegistrado(txtCorreo.getText());
                if (!correoExiste) {
                    JOptionPane.showMessageDialog(null, "El correo ya se encuentra registrado", "ERROR", JOptionPane.ERROR_MESSAGE);
                    return; // Salir del método sin realizar el registro
                }

                //registra la cuenta
                cdao.getPersona().setEstado(false); // estado del votante si ya voto o no false=no Y true=  ya voto 
                cdao.getPersona().setNombres(txtNombre.getText().toLowerCase());
                cdao.getPersona().setApellidos(txtApellido.getText().toLowerCase());
                cdao.getPersona().setEdad(Integer.parseInt(jspEdad.getValue().toString()));
                cdao.getPersona().setSexo(cbxSexo.getSelectedItem().toString());
                cdao.getPersona().setCorreo(txtCorreo.getText());
                cdao.getPersona().setCedula(txtCedula.getText());
                cdao.getPersona().setContrasena(cdao.encriptarContrasena(txtPassword.getText())); // Guardar la contraseña encriptada en lugar de la original

                if (cdao.getPersona().getId() != null) {
                    // En alguna instancia se podria poder hacer que modifique la cuenta
                } else {
                    cdao.guardar();
                }

                JOptionPane.showMessageDialog(null, "Usted se ha registrado correctamente", "Message", JOptionPane.INFORMATION_MESSAGE);
                limpiar();
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Error al registrarse, informacion ya registrada, verifique sus datos", "ERROR", JOptionPane.ERROR_MESSAGE);
            }

        } else {
            JOptionPane.showMessageDialog(null, "Ingrese correctamente todos los datos correspondientes", "ERROR", JOptionPane.ERROR_MESSAGE);
        }
    }

    public FrmRegistrar(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jPanel1 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txtApellido = new javax.swing.JTextField();
        txtCedula = new javax.swing.JTextField();
        txtPassword = new javax.swing.JPasswordField();
        chkMostrar = new javax.swing.JCheckBox();
        btnRegistrar = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        txtRepetirPassword = new javax.swing.JPasswordField();
        jLabel2 = new javax.swing.JLabel();
        btnCuentaExistente = new javax.swing.JButton();
        txtNombre = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        txtCorreo = new javax.swing.JTextField();
        jspEdad = new javax.swing.JSpinner();
        cbxSexo = new javax.swing.JComboBox<>();
        jLabel9 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jTextField1 = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(null);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setLayout(null);

        jLabel3.setBackground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Nombres :");
        jPanel1.add(jLabel3);
        jLabel3.setBounds(30, 60, 80, 16);

        jLabel4.setText("Apellidos :");
        jPanel1.add(jLabel4);
        jLabel4.setBounds(30, 110, 80, 20);

        jLabel5.setText("Cedula :");
        jPanel1.add(jLabel5);
        jLabel5.setBounds(90, 300, 60, 20);

        jLabel6.setText("Contrasena :");
        jPanel1.add(jLabel6);
        jLabel6.setBounds(90, 350, 90, 20);

        jLabel7.setText("Correo electronico:");
        jPanel1.add(jLabel7);
        jLabel7.setBounds(20, 240, 130, 20);

        txtApellido.setForeground(new java.awt.Color(51, 51, 51));
        jPanel1.add(txtApellido);
        txtApellido.setBounds(70, 130, 250, 30);

        txtCedula.setForeground(new java.awt.Color(51, 51, 51));
        txtCedula.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCedulaActionPerformed(evt);
            }
        });
        jPanel1.add(txtCedula);
        txtCedula.setBounds(150, 320, 170, 22);

        txtPassword.setForeground(new java.awt.Color(51, 51, 51));
        jPanel1.add(txtPassword);
        txtPassword.setBounds(150, 370, 170, 22);

        chkMostrar.setBackground(new java.awt.Color(255, 255, 255));
        chkMostrar.setForeground(new java.awt.Color(101, 148, 148));
        chkMostrar.setText("Mostrar contrasena");
        chkMostrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkMostrarActionPerformed(evt);
            }
        });
        jPanel1.add(chkMostrar);
        chkMostrar.setBounds(190, 450, 140, 20);

        btnRegistrar.setBackground(new java.awt.Color(76, 91, 130));
        btnRegistrar.setForeground(new java.awt.Color(255, 255, 255));
        btnRegistrar.setText("Registrar");
        btnRegistrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegistrarActionPerformed(evt);
            }
        });
        jPanel1.add(btnRegistrar);
        btnRegistrar.setBounds(120, 490, 100, 23);

        jLabel1.setText("Repetir contrasena :");
        jPanel1.add(jLabel1);
        jLabel1.setBounds(90, 400, 130, 16);

        txtRepetirPassword.setForeground(new java.awt.Color(51, 51, 51));
        jPanel1.add(txtRepetirPassword);
        txtRepetirPassword.setBounds(150, 420, 170, 22);

        jLabel2.setFont(new java.awt.Font("Ubuntu Mono", 0, 48)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(76, 91, 130));
        jLabel2.setText("Registro");
        jPanel1.add(jLabel2);
        jLabel2.setBounds(70, 0, 210, 70);

        btnCuentaExistente.setBackground(new java.awt.Color(76, 91, 130));
        btnCuentaExistente.setForeground(new java.awt.Color(255, 255, 255));
        btnCuentaExistente.setText("Ya tengo una cuenta");
        btnCuentaExistente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCuentaExistenteActionPerformed(evt);
            }
        });
        jPanel1.add(btnCuentaExistente);
        btnCuentaExistente.setBounds(20, 560, 170, 23);

        txtNombre.setForeground(new java.awt.Color(51, 51, 51));
        txtNombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNombreActionPerformed(evt);
            }
        });
        jPanel1.add(txtNombre);
        txtNombre.setBounds(70, 80, 250, 30);

        jLabel8.setBackground(new java.awt.Color(0, 0, 0));
        jLabel8.setText("Edad:");
        jPanel1.add(jLabel8);
        jLabel8.setBounds(30, 160, 60, 20);

        txtCorreo.setForeground(new java.awt.Color(51, 51, 51));
        jPanel1.add(txtCorreo);
        txtCorreo.setBounds(70, 260, 250, 30);

        jspEdad.setModel(new javax.swing.SpinnerNumberModel(16, 16, 120, 1));
        jPanel1.add(jspEdad);
        jspEdad.setBounds(230, 170, 90, 22);

        cbxSexo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Masculino", "Femenino" }));
        jPanel1.add(cbxSexo);
        cbxSexo.setBounds(200, 210, 120, 22);

        jLabel9.setText("Sexo:");
        jPanel1.add(jLabel9);
        jLabel9.setBounds(30, 200, 60, 16);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(320, 0, 350, 620);

        jPanel2.setBackground(new java.awt.Color(76, 91, 130));
        jPanel2.setLayout(null);

        jTextField1.setEditable(false);
        jTextField1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.add(jTextField1);
        jTextField1.setBounds(210, 490, 120, 22);

        jLabel10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/images/principal.png"))); // NOI18N
        jPanel2.add(jLabel10);
        jLabel10.setBounds(60, 180, 170, 180);

        getContentPane().add(jPanel2);
        jPanel2.setBounds(0, 0, 320, 620);

        setSize(new java.awt.Dimension(668, 648));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents


    private void chkMostrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkMostrarActionPerformed
        mostrarPassword();
    }//GEN-LAST:event_chkMostrarActionPerformed

    private void btnRegistrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegistrarActionPerformed
        save();

    }//GEN-LAST:event_btnRegistrarActionPerformed

    private void btnCuentaExistenteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCuentaExistenteActionPerformed
        //   new FrmRegistrar(null, true).setVisible(false);
        this.dispose();
        new FrmLogin(null, true).setVisible(true);

    }//GEN-LAST:event_btnCuentaExistenteActionPerformed

    private void txtCedulaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCedulaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCedulaActionPerformed

    private void txtNombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNombreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNombreActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmRegistrar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmRegistrar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmRegistrar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmRegistrar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                FrmRegistrar dialog = new FrmRegistrar(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCuentaExistente;
    private javax.swing.JButton btnRegistrar;
    private javax.swing.JComboBox<String> cbxSexo;
    private javax.swing.JCheckBox chkMostrar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JSpinner jspEdad;
    private javax.swing.JTextField txtApellido;
    private javax.swing.JTextField txtCedula;
    private javax.swing.JTextField txtCorreo;
    private javax.swing.JTextField txtNombre;
    private javax.swing.JPasswordField txtPassword;
    private javax.swing.JPasswordField txtRepetirPassword;
    // End of variables declaration//GEN-END:variables
}
