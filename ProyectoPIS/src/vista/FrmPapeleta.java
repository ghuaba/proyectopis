package vista;

import controlador.dao.CandidatoDao;
import controlador.dao.DignidadDao;
import controlador.dao.EleccionDAO;
import controlador.dao.PapeletaDAO;
import controlador.dao.PersonaDAO;
import controlador.dao.VotoDao;
import controlador.ed.lista.ListaEnlazada;
import java.awt.Color;
import java.util.Calendar;

import java.util.HashMap;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import modelo.Candidato;
import modelo.Dignidad;
import modelo.Eleccion;
import modelo.Papeleta;
import modelo.PapeletaComponent;
import modelo.Persona;
import modelo.Voto;

/**
 *
 * @author
 */
public class FrmPapeleta extends javax.swing.JDialog {

    private VotoDao vd = new VotoDao();
    private CandidatoDao cd = new CandidatoDao();
    private DignidadDao dd = new DignidadDao();
    private PapeletaComponent pc = new PapeletaComponent();
    private PersonaDAO pdao = new PersonaDAO();
    private PapeletaDAO papedao = new PapeletaDAO();
    private Persona persona;
    private Eleccion eleccion;
    private Voto voto;
    EleccionDAO edao = new EleccionDAO();
    Calendar fechaHoraActual = Calendar.getInstance();                       //valido

    public FrmPapeleta(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        getContentPane().setBackground(new Color(240, 240, 240)); // Agrega un color de fondo suave
        addTabs();
    }

    /**
     * Constructor de la ventana FrmPapeleta.
     *
     * @param parent El componente padre de la ventana.
     * @param modal Indica si la ventana es modal o no.
     * @param persona Objeto que representa a la persona que realiza el voto.
     * @param eleccion Objeto que representa la elección en la que se está
     * votando.
     */
    public FrmPapeleta(java.awt.Frame parent, boolean modal, Persona persona, Eleccion eleccion) {
        super(parent, modal);
        initComponents(); // Inicializa los componentes visuales
        addTabs(); // Agrega las pestañas correspondientes a la papeleta
        getContentPane().setBackground(new Color(240, 240, 240)); // Establece un color de fondo suave
        lblFecha.setText("Fecha actual: " + fechaHoraActual.getTime()); // Muestra la fecha actual en un label

        // Asigna los objetos persona y elección a los atributos de la clase
        this.persona = persona;
        this.eleccion = eleccion;
        if (persona.getEstado() || !eleccion.getEstado()) {
            btnGuardarVoto.setEnabled(false);
        }
    }

    /**
     * Agrega pestañas al componente "tabbed" con base en la construcción de la
     * papeleta.
     *
     * Este método construye y agrega las pestañas correspondientes a la
     * papeleta de votación, utilizando los datos proporcionados en los objetos
     * "dd" y "cd".
     */
    private void addTabs() {
        try {
            // Construir y agregar pestañas a la papeleta utilizando los objetos "dd" y "cd"
            pc.construirPapeleta(tabbed, dd, cd);
        } catch (Exception e) {
            // Manejar cualquier error que pueda ocurrir durante la construcción de las pestañas
            System.out.println("Error: " + e);
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, e.getMessage(), "Error al agregar pestañas", JOptionPane.ERROR_MESSAGE);
        }
    }

    @SuppressWarnings("unchecked")


    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        tabbed = new javax.swing.JTabbedPane();
        jLabel1 = new javax.swing.JLabel();
        lblFecha = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        btnGuardarVoto = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(null);

        jPanel3.setBackground(new java.awt.Color(54, 87, 120));
        jPanel3.setLayout(null);

        jPanel1.setBackground(new java.awt.Color(27, 48, 75));
        jPanel1.setLayout(null);

        tabbed.setBackground(new java.awt.Color(102, 102, 255));
        tabbed.setForeground(new java.awt.Color(255, 255, 102));
        jPanel1.add(tabbed);
        tabbed.setBounds(40, 100, 860, 310);

        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("     PAPELETA");
        jPanel1.add(jLabel1);
        jLabel1.setBounds(400, 50, 100, 50);

        lblFecha.setForeground(new java.awt.Color(255, 255, 255));
        jPanel1.add(lblFecha);
        lblFecha.setBounds(30, 20, 330, 50);

        jPanel3.add(jPanel1);
        jPanel1.setBounds(30, 20, 940, 460);

        jPanel2.setBackground(new java.awt.Color(204, 204, 204));
        jPanel2.setLayout(null);

        btnGuardarVoto.setBackground(new java.awt.Color(54, 87, 120));
        btnGuardarVoto.setForeground(new java.awt.Color(255, 255, 255));
        btnGuardarVoto.setText("Guarda Voto");
        btnGuardarVoto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarVotoActionPerformed(evt);
            }
        });
        jPanel2.add(btnGuardarVoto);
        btnGuardarVoto.setBounds(380, 20, 140, 26);

        jPanel3.add(jPanel2);
        jPanel2.setBounds(30, 500, 940, 70);

        getContentPane().add(jPanel3);
        jPanel3.setBounds(0, 0, 1010, 610);

        setSize(new java.awt.Dimension(1008, 637));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents
/**
     * Acción ejecutada cuando se hace clic en el botón "jButton1". Realiza el
     * proceso de registro de voto y actualización de información de la persona
     * que votó.
     *
     * @param evt El evento de acción que activa este método.
     */
    private void btnGuardarVotoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarVotoActionPerformed

        HashMap<Integer, String> mapa = pc.getMap();

        if (persona.getEstado() || !eleccion.getEstado()) {
            mostrarMensaje("Su voto no se ha registrado, el tiempo de elección ha finalizado");
            mostrarFrmCertPresentacion();
        } else {
            try {
                vd.votar(vd.listaVotoGuardar(mapa, eleccion)); //valaaa

                configurarVoto();
                JOptionPane.showMessageDialog(null, "Usted ha realizado su voto", "Ok", JOptionPane.INFORMATION_MESSAGE);
                this.setModal(false);
                mostrarFrmCertificado();
            } catch (Exception e) {
                manejarError(e);
            }
        }

    }//GEN-LAST:event_btnGuardarVotoActionPerformed

    private void mostrarMensaje(String mensaje) {
        JOptionPane.showMessageDialog(null, mensaje);
    }

    private void mostrarFrmCertPresentacion() {
        this.dispose();
        new FrmCertPresentacion(null, true, persona, eleccion).setVisible(true);
    }

    private void mostrarFrmCertificado() {
        this.dispose();
        new FrmCertificado(null, true, persona, eleccion).setVisible(true);
    }

    private void configurarVoto() {

        try {
            int generar = papedao.generarNumeroPapeletaUnico();
            papedao.getPapeleta().setFecha(fechaHoraActual.getTime());                //valido
            papedao.getPapeleta().setNro(generar); //valido
            papedao.guardar();//valido
            papedao.setPapeleta(null); //valido

            Papeleta papeleta = papedao.buscarPapeletaPorNumero(generar);
            pdao.setPersona(pdao.obtener(persona.getId())); //vadlio
            pdao.getPersona().setEstado(true); //valido
            pdao.getPersona().setId_papeleta(papeleta.getId());
            pdao.modificar();// sucede debido a que en base de datos no contiene contiene parametrop de posicion //valiod
            pdao.setPersona(null);
        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, "Erro, no se pudo registrar su papeleta como usuario");
        }
    }

    private void manejarError(Exception e) {
        System.out.println("Hay un error: " + e);
    }

    /**
     * persona.setId_papeleta(cb.getPapeleta().getId()); pdao.modificar();
     *
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                FrmPapeleta dialog = new FrmPapeleta(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnGuardarVoto;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JLabel lblFecha;
    private javax.swing.JTabbedPane tabbed;
    // End of variables declaration//GEN-END:variables
}
