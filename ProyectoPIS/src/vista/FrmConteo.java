/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JDialog.java to edit this template
 */
package vista;

import controlador.dao.EleccionDAO;
import controlador.dao.VotoDao;
import controlador.ed.lista.ListaEnlazada;
import controlador.ed.lista.exception.PosicionException;
import controlador.exception.VacioException;
import java.awt.Dimension;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextField;
import modelo.Candidato;
import modelo.Eleccion;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

/**
 *
 * @author ubuntu
 */
public class FrmConteo extends javax.swing.JDialog {

    private DefaultCategoryDataset dataset;
    private JFreeChart chart;
    private JTextField luisaField, jeanField, javirField;
    private int currentValue, currentValue2, currentValue3;
    private VotoDao votoDAO = new VotoDao();
    ListaEnlazada<Candidato> cand = new ListaEnlazada<>();

    /**
     * Creates new form FrmConteo
     */
    public FrmConteo(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        // dataset = new DefaultCategoryDataset();
    }

    public FrmConteo(String labelText) {
//        initComponents();
//        lblname.setText(labelText);
//        if (labelText.equals("ALCALDE") || labelText.equals("PREFECTO") || labelText.equals("CONSEJAL")) {
//            btnvolver.setEnabled(false);
//        }
    }

    /**
     * Se extrae los votos nulos de la base de datos y se muestra en el Lable de
     * "lblNulo" para indicar cuantos son nulos
     */
    public void mostrarVotosNulos() {
        try {
            // Establecer la conexión a la base de datos
//            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/voto_electronico", "root", "Dazuxzu2312.");
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/voto_electronico", "root", "Pool123");

            // Crear la consulta SQL
            String sql = "SELECT COUNT(*) AS votos_nulos FROM voto WHERE tipo = 'NULO'";

            // Crear un PreparedStatement para ejecutar la consulta
            PreparedStatement preparedStatement = connection.prepareStatement(sql);

            // Ejecutar la consulta y obtener el resultado
            ResultSet resultSet = preparedStatement.executeQuery();

            // Obtener la cantidad de votos nulos
            int votosNulos = 0;
            if (resultSet.next()) {
                votosNulos = resultSet.getInt("votos_nulos");
            }

            // Mostrar la cantidad de votos nulos en el label
            lblNulos.setText("Votos nulos: " + votosNulos);

            // Cerrar recursos
            resultSet.close();
            preparedStatement.close();
            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
            // Manejo de errores
        }
    }

    /**
     * Se extrae los votos en blanco de la base de datos y se muestra en el
     * Lable de "lblBlanco" para indicar cuantos votos son en blanco
     */
    public void mostrarVotosEnBlanco() {
        try {
            // Establecer la conexión a la base de datos
//            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/voto_electronico", "root", "Dazuxzu2312.");
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/voto_electronico", "root", "Pool123");

            // Crear la consulta SQL
            String sql = "SELECT COUNT(*) AS votos_en_blanco FROM voto WHERE tipo = 'BLANCO'";

            // Crear un PreparedStatement para ejecutar la consulta
            PreparedStatement preparedStatement = connection.prepareStatement(sql);

            // Ejecutar la consulta y obtener el resultado
            ResultSet resultSet = preparedStatement.executeQuery();

            // Obtener la cantidad de votos en blanco
            int votosEnBlanco = 0;
            if (resultSet.next()) {
                votosEnBlanco = resultSet.getInt("votos_en_blanco");
            }

            // Mostrar la cantidad de votos en blanco en el label
            lblBlancos.setText("Votos en blanco: " + votosEnBlanco);

            // Cerrar recursos
            resultSet.close();
            preparedStatement.close();
            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
            // Manejo de errores
        }
    }

    /**
     * Se extrae el total de votantes de la base de datos y se muestra en el
     * Lable de "lblVotantes" para indicar cuantos votantes en total hicieron su
     * sufragio
     */
    public void mostrarUsuariosQueVotaron() {
        try {
            // Establecer la conexión a la base de datos
//            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/voto_electronico", "root", "Dazuxzu2312.");
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/voto_electronico", "root", "Pool123");

            // Crear la consulta SQL
            String sql = "SELECT nombre FROM usuarios WHERE ha_votado = true";

            // Crear un statement para ejecutar la consulta
            Statement statement = connection.createStatement();

            // Ejecutar la consulta y obtener el resultado
            ResultSet resultSet = statement.executeQuery(sql);

            // Construir la cadena con los nombres de los usuarios que votaron
            StringBuilder usuariosVotaron = new StringBuilder();
            while (resultSet.next()) {
                String nombreUsuario = resultSet.getString("nombre");
                usuariosVotaron.append(nombreUsuario).append(", ");
            }

            // Mostrar los nombres en el label (quitando la última coma y espacio)
            lblVotantes.setText("Usuarios que votaron: " + usuariosVotaron.toString().replaceAll(", $", ""));

            // Cerrar recursos
            resultSet.close();
            statement.close();
            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
            // Manejo de errores
        }
    }

    /**
     * Se extrae los votos validos de la base de datos con su respectivo
     * candidato, se selecciona el boton: "btnEstaadistica" para mostrar el
     * Diagrama de barras con los resultados de las votaciones y su respectivo
     * ganador.
     */
    public void createChart(ListaEnlazada<Candidato> candidatos) throws VacioException, PosicionException, controlador.exception.VacioException, controlador.exception.PosicionException, controlador.ed.lista.exception.VacioException {

        dataset.clear();
        EleccionDAO elecdap = new EleccionDAO();
        Eleccion elecciond = elecdap.busquedaBinariaIdentificador(12345);

        // Se obtiene el candidato 
        for (int i = 0; i < candidatos.size(); i++) {
            Candidato candidato = candidatos.get(i);
            int currentValue = votoDAO.getContadorVotosPorCandidato(candidato.getId()); // Obtén el contador de votos por candidato
            dataset.addValue(currentValue, candidato.getId_persona(), candidato.getId_partido());
        }
//        for (Candidato candidato : candidatos) {
//            int currentValue = votoDAO.listaVotoGuardar(candidato.getId(), elecciond); // Obtén el contador de votos por candidato
//            dataset.addValue(currentValue, candidato.getId_persona(), candidato.getId_partido());
//        }

        //Diagrama de Barras  (titulos)
        chart = ChartFactory.createBarChart(
                "Candidatos",
                "Votaciones",
                "Conteo",
                dataset,
                PlotOrientation.HORIZONTAL,
                true,
                true,
                false
        );

        // Se crea un panel para para el diagrama 
        ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new Dimension(800, 600));
        setContentPane(chartPanel);
        revalidate(); // Refresh the content
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        lblVotantes = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        lblBlancos = new javax.swing.JLabel();
        lblNulos = new javax.swing.JLabel();
        btnvolver = new javax.swing.JButton();
        btnsiguiente = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        lblname = new javax.swing.JLabel();
        btnEstaditica = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Conteo Votaciones", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Liberation Sans", 0, 18), new java.awt.Color(0, 0, 0))); // NOI18N

        jLabel2.setBackground(new java.awt.Color(0, 0, 0));
        jLabel2.setFont(new java.awt.Font("Waree", 1, 12)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(0, 0, 0));
        jLabel2.setText("Numero total de Votantes:");

        lblVotantes.setForeground(new java.awt.Color(102, 102, 102));
        lblVotantes.setText("<votantes>");

        jLabel4.setFont(new java.awt.Font("Waree", 1, 12)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(0, 0, 0));
        jLabel4.setText("NULOS");

        jLabel5.setFont(new java.awt.Font("Waree", 1, 12)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(0, 0, 0));
        jLabel5.setText("BLANCOS");

        lblBlancos.setForeground(new java.awt.Color(102, 102, 102));
        lblBlancos.setText("<blancos>");

        lblNulos.setForeground(new java.awt.Color(102, 102, 102));
        lblNulos.setText("<nulos>");

        btnvolver.setText("Volver");
        btnvolver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnvolverActionPerformed(evt);
            }
        });

        btnsiguiente.setText("Siguiente Dignidad");
        btnsiguiente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsiguienteActionPerformed(evt);
            }
        });

        jPanel2.setBackground(new java.awt.Color(76, 91, 130));

        lblname.setFont(new java.awt.Font("Waree", 1, 12)); // NOI18N
        lblname.setText("jLabel1");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(280, 280, 280)
                .addComponent(lblname)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addComponent(lblname)
                .addContainerGap(18, Short.MAX_VALUE))
        );

        btnEstaditica.setText("Mostrar Estadistica");
        btnEstaditica.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEstaditicaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 273, Short.MAX_VALUE)
                        .addComponent(lblVotantes))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addGap(67, 67, 67)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addComponent(lblNulos))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblBlancos)
                            .addComponent(jLabel5))))
                .addGap(116, 116, 116))
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnsiguiente)
                .addGap(113, 113, 113)
                .addComponent(btnEstaditica)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnvolver)
                .addGap(30, 30, 30))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(lblVotantes))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5))
                .addGap(32, 32, 32)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblBlancos)
                    .addComponent(lblNulos))
                .addGap(25, 25, 25)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnsiguiente)
                    .addComponent(btnvolver)
                    .addComponent(btnEstaditica))
                .addContainerGap(12, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * boton para volver al frm principal despues de ver los resultados
     */
        private void btnvolverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnvolverActionPerformed
            // TODO add your handling code here:
            FrmPrincipal frmpri = new FrmPrincipal(null, rootPaneCheckingEnabled);
            this.setVisible(false);
            frmpri.setVisible(true);
        }//GEN-LAST:event_btnvolverActionPerformed

    /**
     * Boton para mostrar la siguiente dignidad con sus resultados
     */
        private void btnsiguienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsiguienteActionPerformed
            // TODO add your handling code here:
            String nuevoTexto = "";
            // Obtener el texto actual del Label

            String textoActual = lblname.getText();

            // Determinar el nuevo texto según el texto actual
            switch (textoActual) {
                case "ALCALDE":
                    nuevoTexto = "PREFECTO";
                    break;
                case "PREFECTO":
                    nuevoTexto = "CONSEJAL";
                    break;
                case "CONSEJAL":
                    nuevoTexto = "CONTEO TERMINADO";
                    break;
                default:
                    nuevoTexto = "ALCALDE";
                    break;
            }

            // Crear una nueva instancia del formulario y pasar el nuevo texto
            this.dispose();
            FrmConteo nuevoFormulario = new FrmConteo(nuevoTexto);
            if (nuevoTexto.equals("ALCALDE") || nuevoTexto.equals("PREFECTO") || nuevoTexto.equals("CONSEJAL")) {
                nuevoFormulario.btnvolver.setEnabled(false);
            }
            if (nuevoTexto.equals("CONTEO")) {
                nuevoFormulario.btnsiguiente.setEnabled(false);
            } else {
                nuevoFormulario.btnsiguiente.setEnabled(true);
            }

            nuevoFormulario.setVisible(true);
        }//GEN-LAST:event_btnsiguienteActionPerformed
        
    /**
     * Boton para mostrar el panel con el diagrama de barras
     */    
    private void btnEstaditicaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEstaditicaActionPerformed
        try {
            // TODO add your handling code here:
            createChart(cand);
        } catch (VacioException ex) {
            Logger.getLogger(FrmConteo.class.getName()).log(Level.SEVERE, null, ex);
        } catch (PosicionException ex) {
            Logger.getLogger(FrmConteo.class.getName()).log(Level.SEVERE, null, ex);
        } catch (controlador.exception.PosicionException ex) {
            Logger.getLogger(FrmConteo.class.getName()).log(Level.SEVERE, null, ex);
        } catch (controlador.ed.lista.exception.VacioException ex) {
            Logger.getLogger(FrmConteo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnEstaditicaActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmConteo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmConteo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmConteo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmConteo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                FrmConteo dialog = new FrmConteo(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnEstaditica;
    private javax.swing.JButton btnsiguiente;
    private javax.swing.JButton btnvolver;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel lblBlancos;
    private javax.swing.JLabel lblNulos;
    private javax.swing.JLabel lblVotantes;
    private javax.swing.JLabel lblname;
    // End of variables declaration//GEN-END:variables
}
