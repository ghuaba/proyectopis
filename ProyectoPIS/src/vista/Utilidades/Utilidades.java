/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vista.Utilidades;


import controlador.dao.DignidadDao;
import controlador.ed.lista.ListaEnlazada;
import javax.swing.JComboBox;

import modelo.Dignidad;
import modelo.TipoC;

/**
 *
 * @author mt3k
 */
public class Utilidades {

    public static void cargarDignidad(JComboBox cbx, DignidadDao dd) throws Exception {//AQUI SE CARGA EL COMBO CON LAS DIGNIDADES
        cbx.removeAllItems();

        ListaEnlazada<Dignidad> lista = dd.ordenarDignidad(dd.listar());

        for (int i = 0; i < lista.size(); i++) {
            cbx.addItem(lista.get(i).getNombre().toUpperCase());
        }
    }

     public static void cargarTipoCandidato(JComboBox comboBox) {
        comboBox.removeAllItems();

        for (TipoC value : TipoC.values()) {
            comboBox.addItem(value);
        }
    
     }
}
