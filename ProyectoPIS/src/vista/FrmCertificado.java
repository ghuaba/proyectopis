// prueba
package vista;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import static javax.servlet.jsp.PageContext.PAGE;
import javax.swing.JButton;
import modelo.Eleccion;
import modelo.Persona;

/**
 *
 * @author FA506ICB-HN114W
 */
public class FrmCertificado extends javax.swing.JDialog {

    private Persona persona;
    private Eleccion eleccion;
    LocalDate fechaActual = LocalDate.now();

    public FrmCertificado(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
//        fecha.setText("Fecha de Emision: " + fechaActual);
    }

    public FrmCertificado(java.awt.Frame parent, boolean modal, Persona persona, Eleccion eleccion) {
        super(parent, modal);
        initComponents();

        this.persona = persona;
        this.eleccion = eleccion;
        loads();

    }

// Establecer los textos en los JLabel
    private void loads() {
        fecha.setText("Fecha de Emision: " + fechaActual);
        lblNombre.setText("Nombre: " + persona.getNombres());
        lblApellido.setText("Apellidos: " + persona.getApellidos());
        lblCedula.setText("CC No: " + persona.getCedula());

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        lblNombre = new javax.swing.JLabel();
        btnSalir = new javax.swing.JButton();
        lblCedula = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        fecha = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        lblApellido = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(76, 91, 130));
        jPanel1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel1.setForeground(new java.awt.Color(255, 0, 51));
        jPanel1.setLayout(null);

        lblNombre.setFont(new java.awt.Font("Waree", 2, 14)); // NOI18N
        lblNombre.setForeground(new java.awt.Color(255, 255, 255));
        lblNombre.setText("Nombre");
        jPanel1.add(lblNombre);
        lblNombre.setBounds(180, 150, 210, 30);

        btnSalir.setBackground(new java.awt.Color(76, 91, 130));
        btnSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/images/puerta-de-salida.png"))); // NOI18N
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });
        jPanel1.add(btnSalir);
        btnSalir.setBounds(660, 390, 30, 30);

        lblCedula.setFont(new java.awt.Font("Waree", 2, 14)); // NOI18N
        lblCedula.setForeground(new java.awt.Color(255, 255, 255));
        lblCedula.setText("Cédula");
        jPanel1.add(lblCedula);
        lblCedula.setBounds(250, 280, 180, 30);

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/images/hombre.png"))); // NOI18N
        jPanel1.add(jLabel4);
        jLabel4.setBounds(380, 120, 130, 130);

        fecha.setFont(new java.awt.Font("Rockstar Extra Bold", 0, 14)); // NOI18N
        fecha.setForeground(new java.awt.Color(255, 255, 255));
        jPanel1.add(fecha);
        fecha.setBounds(100, 70, 212, 22);

        jLabel6.setFont(new java.awt.Font("Rockstar Extra Bold", 0, 12)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("SALIR");
        jPanel1.add(jLabel6);
        jLabel6.setBounds(650, 370, 50, 15);

        jLabel1.setFont(new java.awt.Font("Rockstar Extra Bold", 1, 24)); // NOI18N
        jLabel1.setText("CERTIFICADO DE VOTACION");

        jLabel7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/images/2560px-CNE_Ecuador.svg (1).png"))); // NOI18N

        jLabel8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/vista/images/Coat_of_arms_of_Ecuador.svg (1).png"))); // NOI18N

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(jLabel7)
                .addGap(96, 96, 96)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 116, Short.MAX_VALUE)
                .addComponent(jLabel8)
                .addGap(17, 17, 17))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, 60, Short.MAX_VALUE)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel1.add(jPanel2);
        jPanel2.setBounds(0, 0, 720, 60);
        jPanel1.add(jLabel9);
        jLabel9.setBounds(460, 170, 0, 0);

        lblApellido.setFont(new java.awt.Font("Waree", 2, 14)); // NOI18N
        lblApellido.setForeground(new java.awt.Color(255, 255, 255));
        lblApellido.setText("Apellido");
        jPanel1.add(lblApellido);
        lblApellido.setBounds(180, 220, 190, 30);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 722, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 430, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        setSize(new java.awt.Dimension(732, 460));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed
        this.dispose();

    }//GEN-LAST:event_btnSalirActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                FrmCertificado dialog = new FrmCertificado(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnSalir;
    private javax.swing.JLabel fecha;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel lblApellido;
    private javax.swing.JLabel lblCedula;
    private javax.swing.JLabel lblNombre;
    // End of variables declaration//GEN-END:variables

}
