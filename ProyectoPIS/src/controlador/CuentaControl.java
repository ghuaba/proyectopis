//mensaje para git 
// mensaje desde la web de el repositorio
package controlador;

import controlador.ed.lista.ListaEnlazada;
import modelo.Persona;

/**
 *
 * @author ghuaba
 */
public class CuentaControl {
    
     
    
   private ListaEnlazada<Persona> cuentas;
//    private ListaEnlazada<Venta> venta;
    private Persona cuenta;
//    private Venta ventas;
    
    
    
    
    
    
    
 
    private int id = 0;

    private ListaEnlazada<Persona> listaCuentas = new ListaEnlazada<>();
    
  

    public ListaEnlazada<Persona> getCuentas() {
        return cuentas;
    }

    public void setCuentas(ListaEnlazada<Persona> cuentas) {
        this.cuentas = cuentas;
    }

    public Persona getCuenta() {
         if (cuenta == null) // se crea por si necesito memoria
        {
            cuenta = new Persona();
        }
        return cuenta;
    }

    public void setCuenta(Persona cuenta) {
        this.cuenta = cuenta;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ListaEnlazada<Persona> getListaCuentas() {
        return listaCuentas;
    }

    public void setListaCuentas(ListaEnlazada<Persona> listaCuentas) {
        this.listaCuentas = listaCuentas;
    }

  
    
     
}
