package controlador.dao;

import controlador.ed.lista.ListaEnlazada;

import modelo.Candidato;

/**
 *
 * @author mt3k
 */
public class CandidatoDao extends AdaptadorDAO<Candidato> {

    private Candidato candidato;

    public CandidatoDao() {
        super(Candidato.class);
    }

    public Integer guardar() throws Exception {
        return this.guardar(candidato);

    }

    public boolean modificar() throws Exception {
        this.modificar(candidato);
        return true;

    }

    private Integer generateID() {
        return listar().size() + 1;
    }

    public Candidato getCandidato() {
        if (this.candidato == null) {
            this.candidato = new Candidato();
        }
        return candidato;
    }

    public void setCandidato(Candidato candidato) {
        this.candidato = candidato;
    }

    /**
     * Recupera una lista enlazada de candidatos asociados a un partido
     * específico basándose en su ID de partido.
     *
     * @param idPartido El ID del partido del cual se desean obtener los
     * candidatos.
     * @return Una instancia de ListaEnlazada que contiene los candidatos
     * relacionados con el partido.
     * @throws Exception Si ocurre algún problema durante la obtención de los
     * candidatos.
     */
    public ListaEnlazada<Candidato> obtenerCandidatosPorPartido(Integer idPartido) throws Exception {
        ListaEnlazada<Candidato> candidatosPorPartido = new ListaEnlazada<>();

        for (int i = 0; i < listar().size(); i++) {
            Candidato candidato = listar().get(i);
            if (candidato.getId_partido().equals(idPartido)) {
                candidatosPorPartido.insertar(candidato);
            }
        }

        return candidatosPorPartido;
    }

    /**
     * Busca y recupera una lista enlazada de candidatos que corresponden a una
     * determinada dignidad y partido.
     *
     * @param id_dignidad El ID de la dignidad por la que se desea filtrar.
     * @param id_partido El ID del partido por el que se desea filtrar.
     * @return Una instancia de ListaEnlazada que contiene los candidatos que
     * cumplen con los criterios de búsqueda.
     * @throws Exception Si ocurre algún problema durante la búsqueda y
     * recuperación de los candidatos.
     */
    public ListaEnlazada<Candidato> buscarPorDignidadPartido(Integer id_dignidad, Integer id_partido) throws Exception {
        ListaEnlazada<Candidato> lista = new ListaEnlazada<>();
        ListaEnlazada<Candidato> listado = listar();
        for (int i = 0; i < listado.size(); i++) {
            Candidato aux = listado.get(i);
            if (aux.getId_dignidad().intValue() == id_dignidad.intValue() && aux.getId_partido().intValue() == id_partido.intValue()) {
                lista.insertar(aux);
            }
        }
        return lista;
    }
}
