/*
 * Este archivo contiene la definición de la clase PersonaDAO que es responsable de la interacción con la base de datos
 * para la entidad Persona. Contiene métodos para guardar, modificar y validar información de personas.
 */
package controlador.dao;

import controlador.ed.lista.ListaEnlazada;
import controlador.exception.PosicionException;
import controlador.exception.VacioException;
import controlador.ed.lista.NodoLista;
import controlador.ordenacion.Quicksort;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import modelo.Persona;

/**
 * Esta clase se encarga de manejar las operaciones de acceso a la base de datos
 * relacionadas con la entidad Persona. Contiene métodos para guardar,
 * modificar, validar y encriptar información relacionada con personas.
 */
public class PersonaDAO extends AdaptadorDAO<Persona> {

    private Persona persona;

    public PersonaDAO() {
        super(Persona.class);
    }

    public Persona getPersona() {
        if (this.persona == null) {
            this.persona = new Persona();
        }
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public Integer guardar() throws Exception {
        return this.guardar(persona);

    }

    public boolean modificar() throws Exception {
        this.modificar(persona);
        return true;

    }

    public Integer generateID() {
        return listar().size() + 1;
    }

    /**
     * Verifica el acceso de un usuario comparando la cédula y contraseña
     * proporcionadas con las almacenadas en la lista de cuentas.
     *
     * @param listaCuentas La lista de cuentas de usuarios registrados.
     * @param cedula La cédula del usuario que intenta acceder.
     * @param contrasena La contraseña del usuario que intenta acceder.
     * @return true si el acceso es válido, false si no es válido.
     */
    public static boolean verificarAcceso(ListaEnlazada<Persona> listaCuentas, String cedula, String contrasena) {
        // Inicialización del nodo actual para recorrer la lista de cuentas
        NodoLista<Persona> nodoActual = listaCuentas.getCabecera();

        // Recorrido de la lista de cuentas para verificar el acceso
        while (nodoActual != null) {
            Persona cuentaActual = nodoActual.getInfo();

            // Comparación de la cédula y contraseña proporcionadas con las almacenadas (usando método de hash)
            if (cuentaActual.getCedula().equals(cedula) && desencriptarContrasena(contrasena).equals(cuentaActual.getContrasena())) {
                return true; // Acceso válido
            }

            // Avanzar al siguiente nodo en la lista
            nodoActual = nodoActual.getSig();
        }

        return false; // No se encontró una coincidencia de cédula y contraseña
    }

    /**
     * Encripta una contraseña utilizando el algoritmo de hash SHA-256.
     *
     * @param contra La contraseña a encriptar.
     * @return La representación en hexadecimal del hash encriptado de la
     * contraseña.
     */
    public String encriptarContrasena(String contra) {
        try {
            // Obtener una instancia del algoritmo de hash SHA-256
            MessageDigest digest = MessageDigest.getInstance("SHA-256");

            // Calcular el hash de la contraseña
            byte[] hash = digest.digest(contra.getBytes());

            // Convertir el hash en una representación hexadecimal
            StringBuilder hexString = new StringBuilder();
            for (byte b : hash) {
                String hex = Integer.toHexString(0xff & b);
                if (hex.length() == 1) {
                    hexString.append('0');
                }
                hexString.append(hex);
            }

            // Retornar la representación en hexadecimal del hash encriptado
            return hexString.toString();
        } catch (NoSuchAlgorithmException e) {
            // Manejo de error si el algoritmo no es compatible
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Desencripta una contraseña utilizando el algoritmo de hash SHA-256 (en
     * realidad, no desencripta sino compara hashes).
     *
     * @param contrasena El hash en hexadecimal de la contraseña a comparar.
     * @return El hash en hexadecimal de la contraseña proporcionada.
     */
    public static String desencriptarContrasena(String contrasena) {
        try {
            // Obtener una instancia del algoritmo de hash SHA-256
            MessageDigest digest = MessageDigest.getInstance("SHA-256");

            // Calcular el hash de la contraseña proporcionada (en realidad, no desencripta sino compara hashes)
            byte[] hash = digest.digest(contrasena.getBytes());

            // Convertir el hash en una representación hexadecimal
            StringBuilder hexString = new StringBuilder();
            for (byte b : hash) {
                String hex = Integer.toHexString(0xff & b);
                if (hex.length() == 1) {
                    hexString.append('0');
                }
                hexString.append(hex);
            }

            // Retornar el hash en hexadecimal de la contraseña proporcionada
            return hexString.toString();
        } catch (NoSuchAlgorithmException e) {
            // Manejo de error si el algoritmo no es compatible
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Verifica si la cédula ya está registrada en la lista de cuentas.
     *
     * @param cedula La cédula a verificar.
     * @return false si la cédula ya existe en la lista, true si no existe.
     * @throws Exception Excepción general en caso de algún error durante la
     * búsqueda.
     */
    public Boolean cedulaExistentes(String cedula) throws Exception {
        // Obtener la lista de cuentas registradas
        ListaEnlazada<Persona> listaCuentas = listar();

        // Recorrer la lista de cuentas y verificar si la cédula ya existe
        for (int i = 0; i < listaCuentas.size(); i++) {
            Persona cuentaExistente = listaCuentas.get(i);

            if (cuentaExistente.getCedula().equals(cedula)) {
                return false; // La cédula ya está registrada
            }
        }

        return true; // La cédula no existe en la lista de cuentas
    }

    /**
     * Verifica si un correo electrónico ya está registrado en la lista de
     * cuentas.
     *
     * @param correo El correo electrónico a verificar.
     * @return false si el correo ya está registrado, true si no está
     * registrado.
     * @throws Exception Excepción general en caso de algún error durante la
     * búsqueda.
     */
    public boolean correoRegistrado(String correo) throws Exception {
        // Obtener la lista de cuentas registradas
        ListaEnlazada<Persona> listaCuentas = listar();

        // Recorrer la lista de cuentas y verificar si el correo ya está registrado
        for (int i = 0; i < listaCuentas.size(); i++) {
            Persona cuentaExistente = listaCuentas.get(i);

            if (cuentaExistente.getCorreo().equals(correo)) {
                return false; // El correo ya está registrado
            }
        }

        return true; // El correo no está registrado en la lista de cuentas
    }


    /**
     * Valida si una cédula ecuatoriana es válida.
     *
     * @param cedula La cédula a validar.
     * @return true si la cédula es válida, false si no lo es.
     */
    public static boolean validarCedula(String cedula) {
        if (cedula.length() != 10) {
            return false;
        }

        int[] digitos = new int[10];
        for (int i = 0; i < 10; i++) {
            digitos[i] = Character.getNumericValue(cedula.charAt(i));
        }

        // Paso 1: Verificar el tercer dígito
        if (digitos[2] > 6) {
            return false;
        }

        // Paso 2: Multiplicar los primeros nueve dígitos por un factor específico
        int[] multiplicadores = {2, 1, 2, 1, 2, 1, 2, 1, 2};
        int[] productos = new int[9];
        for (int i = 0; i < 9; i++) {
            productos[i] = digitos[i] * multiplicadores[i];
        }

        // Paso 3: Sumar los dígitos de los productos calculados en el paso anterior
        int sumaProductos = 0;
        for (int producto : productos) {
            sumaProductos += producto / 10 + producto % 10;
        }

        // Paso 4: Calcular el dígito verificador
        int digitoVerificador = 10 - (sumaProductos % 10);
        if (digitoVerificador == 10) {
            digitoVerificador = 0;
        }

        // Paso 5: Comparar el dígito verificador calculado con el último dígito de la cédula
        return digitoVerificador == digitos[9];
    }

    /**
     * Realiza una búsqueda binaria en la lista de personas ordenada por cédula,
     * para encontrar una persona por su nombre.
     *
     * @param nombre El nombre de la persona a buscar.
     * @return La persona encontrada, o null si no se encontró ninguna
     * coincidencia.
     * @throws VacioException
     * @throws PosicionException
     * @throws controlador.ed.lista.exception.VacioException
     * @throws controlador.ed.lista.exception.PosicionException
     */
    public Persona busquedaBinariaNombre(String nombre) throws VacioException, PosicionException,
            controlador.ed.lista.exception.VacioException, controlador.ed.lista.exception.PosicionException {
        Persona cantante = null;
        ListaEnlazada<Persona> lista = listar();
        int inicio = 0;
        int fin = lista.size() - 1;
        Quicksort quicksort = new Quicksort();
        lista = (ListaEnlazada<Persona>) quicksort.quicksortCedulaCandidato(lista, true);

        while (inicio <= fin) {
            int medio = inicio + (fin - inicio) / 2;

            cantante = lista.get(medio);
            int comparacion = cantante.getCedula().compareToIgnoreCase(nombre);

            if (comparacion == 0) {
                return cantante; // Persona encontrada
            } else if (comparacion < 0) {
                inicio = medio + 1; // Buscar en la mitad superior
            } else {
                fin = medio - 1; // Buscar en la mitad inferior
            }
        }

        return null; // No se encontró una persona con ese nombre
    }

}
