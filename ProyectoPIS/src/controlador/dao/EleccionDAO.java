/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.dao;

import controlador.ed.lista.ListaEnlazada;
import controlador.ed.lista.exception.PosicionException;
import controlador.ed.lista.exception.VacioException;
import controlador.ordenacion.Quicksort;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import modelo.Eleccion;

/**
 *
 * @author ghuaba
 */
public class EleccionDAO extends AdaptadorDAO<Eleccion> {

    private Eleccion eleccion;

    public EleccionDAO() {
        super(Eleccion.class);
    }

    public Eleccion getEleccion() {
        if (this.eleccion == null) {
            this.eleccion = new Eleccion();
        }
        return eleccion;
    }

    public void setEleccion(Eleccion eleccion) {
        this.eleccion = eleccion;
    }

    public Integer guardar() throws Exception {
        return this.guardar(eleccion);

    }

    public boolean modificar() throws Exception {
        this.modificar(eleccion);
        return true;

    }

    public Integer generateID() {
        return listar().size() + 1;
    }

//  //  public void verificarFechaFin() throws Exception {
//        Calendar now = Calendar.getInstance(); // Fecha y hora actual
//
//        if (eleccion.getFecha_fin() != null && now.getTime().after(eleccion.getFecha_fin())) {
//            eleccion.setEstado(false); // Establecer el estado a false es decir que ya acabo
//            modificar(); // Actualizar en la base de datos
//        }
//    }
//  //  public void verificarFechaFin() throws Exception {
//        Calendar now = Calendar.getInstance(); // Fecha y hora actual
//
//        if (eleccion.getFecha_fin() != null && now.getTime().after(eleccion.getFecha_fin())) {
//            eleccion.setEstado(false); // Establecer el estado a false es decir que ya acabo
//            modificar(); // Actualizar en la base de datos
//        }
//    }
    //Verifica el estado de la eleccion con la fecha actual y la fecha de las elecciones registradas
    //las setea en falso si la eleccion ya esta pasada de fecha
    
    /**
 * Verifica y actualiza los estados de las elecciones según la fecha actual.
 * Si la fecha actual es posterior a la fecha de finalización de una elección, se establece su estado como finalizada.
 * Actualiza las elecciones en la base de datos.
 *
 * @throws Exception Si ocurre algún error durante el proceso de verificación y actualización.
 */

    public void verificarYActualizarEstados() throws Exception {
        Calendar now = Calendar.getInstance(); // Fecha y hora actual

        // Obtener todas las elecciones de la base de datos
        ListaEnlazada<Eleccion> elecciones = listar();

        for (int i = 0; i < elecciones.size(); i++) {
            Eleccion eleccion = elecciones.get(i);

            if (eleccion.getFecha_fin() != null && now.getTime().after(eleccion.getFecha_fin())) {
                eleccion.setEstado(false); // Establecer el estado a false es decir que ya acabó
                modificar(eleccion); // Actualizar en la base de datos
            }
        }
    }

    /**
     * Realiza una búsqueda binaria para encontrar una Elección por su número de
     * identificación.
     *
     * @param nroIdentificador El número de identificación de la Elección que se
     * busca.
     * @return Un objeto Elección que coincide con el número de identificación
     * proporcionado, o null si no se encuentra.
     * @throws VacioException Si la lista de Elecciones está vacía.
     * @throws PosicionException Si ocurre un problema con la posición durante
     * la búsqueda.
     */
    public Eleccion busquedaBinariaIdentificador(int nroIdentificador) throws VacioException, PosicionException, controlador.ed.lista.exception.VacioException, controlador.ed.lista.exception.PosicionException, controlador.exception.VacioException, controlador.exception.PosicionException {
        Eleccion eleccionEncontrada = null;
        ListaEnlazada<Eleccion> lista = listar(); // Supongo que tienes un método para obtener la lista de Eleccion
        int inicio = 0;
        int fin = lista.size() - 1;
        Quicksort quicksort = new Quicksort();
        lista = (ListaEnlazada<Eleccion>) quicksort.quicksortIdentificadorEleccion(lista, true);

        while (inicio <= fin) {
            int medio = inicio + (fin - inicio) / 2;

            eleccionEncontrada = lista.get(medio);
            int comparacion = Integer.compare(eleccionEncontrada.getNro_identificador(), nroIdentificador);

            if (comparacion == 0) {
                return eleccionEncontrada;
            } else if (comparacion < 0) {
                inicio = medio + 1;
            } else {
                fin = medio - 1;
            }
        }

        return null;
    }

    
    //metodo de David de busqueda binaria identificador
    public Eleccion busquedaBinariaIdentificadorD(int nroIdentificador) throws VacioException, PosicionException, controlador.ed.lista.exception.VacioException, controlador.ed.lista.exception.PosicionException, controlador.exception.VacioException, controlador.exception.PosicionException {
        Eleccion eleccionEncontrada = null;
        ListaEnlazada<Eleccion> lista = listar(); // Supongo que tienes un método para obtener la lista de Eleccion
        int inicio = 0;
        int fin = lista.size() - 1;
        Quicksort quicksort = new Quicksort();
        lista = (ListaEnlazada<Eleccion>) quicksort.quicksortIdentificadorEleccion(lista, true);

        while (inicio <= fin) {
            int medio = inicio + (fin - inicio) / 2;

            eleccionEncontrada = lista.get(medio);
            int comparacion = Integer.compare(eleccionEncontrada.getNro_identificador(), nroIdentificador);

            if (comparacion == 0) {
                return eleccionEncontrada;
            } else if (comparacion < 0) {
                inicio = medio + 1;
            } else {
                fin = medio - 1;
            }
        }

        return null;
    }

}
