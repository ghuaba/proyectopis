/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.dao;


import controlador.ed.pila.Pila;
import controlador.exception.TopeException;
import controlador.exception.VacioException;
import java.io.IOException;

/**
 *
 * @author ghuaba
 */
public interface InterfazDAOPila<E> {
    
         /**
     * permite guardar una lista enlazada hacia el el repositorio de datos
     *
     * @param obj
     * @throws IOException
     * @throws controlador.exception.TopeException
     */
    public void push(E obj) throws IOException, TopeException;

    /**
     * permite listar los datos del repositorio de datos
     *
     * @return
     */
    public Pila<E> listar();

    /**
     * permite obtener los datos del repositorio de datos
     *
     * @param id
     * @return
     */
    public E obtener(Integer id);
    /**
     * elimina el primer elemento de la pila hacia el repositorio de datos
     * @throws VacioException 
     */
    public void pop() throws VacioException;
    /**
     * elimina el último elemento de lampila hacia el repositorio de datos
     * @throws VacioException 
     */
    public void eliminarPrimero() throws VacioException;
 
}
