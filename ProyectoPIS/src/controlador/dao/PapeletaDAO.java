/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.dao;

import controlador.ed.lista.ListaEnlazada;
import controlador.ed.lista.exception.PosicionException;
import controlador.exception.VacioException;
import controlador.ordenacion.Quicksort;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import modelo.Papeleta;
import modelo.TipoC;

/**
 *
 * @author ghuaba
 */
public class PapeletaDAO extends AdaptadorDAO<Papeleta> {

    private Papeleta papeleta;
//    private ListaEnlazada<Papeleta> cuentas; //

    public PapeletaDAO() {
        super(Papeleta.class);
    }

    public Papeleta getPapeleta() {
        if (this.papeleta == null) {
            this.papeleta = new Papeleta();
        }
        return papeleta;
    }

    public void setPapeleta(Papeleta papeleta) {
        this.papeleta = papeleta;
    }

    public Integer guardar() throws Exception {
        return this.guardar(papeleta);

    }

    public boolean modificar() throws Exception {
        this.modificar(papeleta);
        return true;

    }

    public Integer generateID() {
        return listar().size() + 1;
    }

    /**
     * Busca una papeleta por su número en una lista de papeletas ordenada.
     *
     * @param numeroPapeleta Número de la papeleta que se desea buscar.
     * @return La papeleta encontrada o null si no se encuentra ninguna.
     * @throws VacioException Excepción lanzada si la lista de papeletas está
     * vacía.
     * @throws PosicionException Excepción lanzada si ocurre un error en la
     * posición de la lista.
     * @throws controlador.exception.VacioException Excepción lanzada si ocurre
     * un error de lista vacía.
     * @throws controlador.exception.PosicionException Excepción lanzada si
     * ocurre un error de posición de lista.
     */
    public Papeleta buscarPapeletaPorNumero(int numeroPapeleta) throws VacioException, PosicionException, controlador.ed.lista.exception.VacioException, controlador.ed.lista.exception.PosicionException, controlador.exception.VacioException, controlador.exception.PosicionException {
        Papeleta papeletaEncontrada = null;

        // Supongo que tienes un método para obtener la lista de Papeletas
        ListaEnlazada<Papeleta> lista = listar();

        int inicio = 0;
        int fin = lista.size() - 1;

        // Asumiendo que tienes un método para ordenar la lista de Papeletas según su número
        Quicksort quicksort = new Quicksort();
        lista = (ListaEnlazada<Papeleta>) quicksort.quicksortNumeroPapeleta(lista, true);

        // Búsqueda binaria en la lista ordenada
        while (inicio <= fin) {
            int medio = inicio + (fin - inicio) / 2;

            papeletaEncontrada = lista.get(medio);
            int comparacion = Integer.compare(papeletaEncontrada.getNro(), numeroPapeleta);

            if (comparacion == 0) {
                return papeletaEncontrada;
            } else if (comparacion < 0) {
                inicio = medio + 1;
            } else {
                fin = medio - 1;
            }
        }

        return null;
    }

    /**
     * Verifica si ya existe una papeleta con el mismo número.
     *
     * @param numeroPapeleta Número de papeleta que se desea verificar.
     * @return true si no existe una papeleta con ese número, false si ya
     * existe.
     * @throws Exception Excepción general en caso de algún error durante la
     * verificación.
     */
    private Boolean numeroPapeletaExistente(int numeroPapeleta) throws Exception {
        ListaEnlazada<Papeleta> listaPapeletas = listar();

        // Iterar a través de la lista de papeletas
        for (int i = 0; i < listaPapeletas.size(); i++) {
            Papeleta papeletaExistente = listaPapeletas.get(i);

            // Verificar si el número de la papeleta existe
            if (papeletaExistente.getNro() == numeroPapeleta) {
                return false; // Ya existe una papeleta con ese número
            }
        }

        return true; // No existe una papeleta con ese número
    }

    /**
     * Genera un número de papeleta único que no esté en uso.
     *
     * @return Un número de papeleta único.
     * @throws Exception Excepción general en caso de algún error durante la
     * generación.
     */
    public int generarNumeroPapeletaUnico() throws Exception {
        Random random = new Random();
        int numeroPapeleta;

        do {
            numeroPapeleta = random.nextInt(10000); // Cambia el rango según tus necesidades
        } while (!numeroPapeletaExistente(numeroPapeleta));

        return numeroPapeleta;
    }

}
