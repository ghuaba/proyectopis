package controlador.dao;

import controlador.ed.lista.ListaEnlazada;
import controlador.exception.PosicionException;
import controlador.exception.VacioException;
import java.io.IOException;

import javax.swing.JOptionPane;
import modelo.Candidato;
import modelo.Partido;

/**
 *
 * @author mt3k
 */
public class PartidoPoliticoDao extends AdaptadorDAO<Partido> {

    private Partido partidoPolitico;

    public PartidoPoliticoDao() {
        super(Partido.class);
    }

    public Partido getPartidoPolitico() {
        if (this.partidoPolitico == null) {
            this.partidoPolitico = new Partido();
        }
        return partidoPolitico;
    }

    public void setPartidoPolitico(Partido partidoPolitico) {
        this.partidoPolitico = partidoPolitico;
    }

//    public void guardar() throws IOException {
//        try {
//            if (existePartidoConNombre(partidoPolitico.getNombre())) {
//                JOptionPane.showMessageDialog(null, "YA HAY UN PARTIDO CON ESE NOMBRE", "ERROR", JOptionPane.ERROR_MESSAGE);
//            } else {
//                partidoPolitico.setId(generateID());
//                this.guardar(partidoPolitico);
//            }
//        } catch (Exception e) {
//            System.out.println("ERROR EN GUARDAR" + e);
//        }
//        partidoPolitico.setId(generateID());
//        this.guardar(partidoPolitico);
//    }
    public Integer guardar() throws Exception {
        return this.guardar(partidoPolitico);

    }

    public boolean modificar() throws Exception {
        this.modificar(partidoPolitico);
        return true;

    }

    private Integer generateID() {
        return listar().size() + 1;
    }

    /**
     * Lista los partidos asociados a una dignidad específica a través de los
     * candidatos registrados.
     *
     * @param id_dignidad El ID de la dignidad por la cual se desean filtrar los
     * partidos.
     * @return Una instancia de ListaEnlazada con los objetos Partido que
     * corresponden a la dignidad especificada.
     * @throws Exception Si ocurre algún problema durante la búsqueda y listado
     * de los partidos.
     */
    public ListaEnlazada<Partido> listaPartidosDignidad(Integer id_dignidad) throws Exception {
        ListaEnlazada<Partido> lista = new ListaEnlazada<>();
        ListaEnlazada<Candidato> listaC = new CandidatoDao().listar();
        for (int i = 0; i < listaC.size(); i++) {
            Candidato c = listaC.get(i);
            System.out.println(c.getId_dignidad().intValue() + "  " + id_dignidad.intValue());
            if (c.getId_dignidad().intValue() == id_dignidad.intValue()) {
                System.out.println("GO");
                if (lista.estaVacia() || !estaLista(c.getId_partido(), lista)) {
                    lista.insertar(obtener(c.getId_partido()));
                }

            }

        }
        return lista;

    }

    private Boolean estaLista(Integer id_partido, ListaEnlazada<Partido> lista) throws Exception {
        for (int i = 0; i < lista.size(); i++) {
            if (lista.get(i).getId().intValue() == id_partido.intValue()) {
                return true;
            }
        }

        return false;

    }

    /**
     * Verifica si ya existe un partido con el nombre proporcionado en la lista
     * de partidos.
     *
     * @param nombre El nombre del partido que se desea verificar.
     * @return true si ya existe un partido con el mismo nombre, false en caso
     * contrario.
     * @throws VacioException Si la lista de partidos está vacía.
     * @throws PosicionException Si ocurre un problema con la posición durante
     * la búsqueda.
     */
    public boolean existePartidoConNombre(String nombre) throws controlador.ed.lista.exception.VacioException, controlador.ed.lista.exception.PosicionException {
        ListaEnlazada<Partido> listaPartidos = listar();

        for (int i = 0; i < listaPartidos.size(); i++) {
            Partido partido = listaPartidos.get(i);
            if (partido.getNombre().equalsIgnoreCase(nombre)) {
                return true; // Ya existe un partido con el mismo nombre
            }
        }

        return false; // No se encontró ningún partido con el mismo nombre
    }
//
//  public boolean candidatoEstaEnPartido(String cedula) throws Exception {
//    ListaEnlazada<PartidoPolitico> listaPartidos = listar();
//
//    for (int i = 0; i < listaPartidos.size(); i++) {
//        Partido partido = listaPartidos.get(i);
//        ListaEnlazada<Candidato> candidatosPartido = partido.getCandidatos();
//        
//        if (candidatosPartido != null) { // Verifica que la lista de candidatos esté inicializada
//            for (int j = 0; j < candidatosPartido.size(); j++) {
//                Candidato c = candidatosPartido.get(j);
//                if (c.getCedula().equalsIgnoreCase(cedula)) {
//                    return true; // El candidato con la cédula especificada ya está en la lista de candidatos de algún partido
//                }
//            }
//        }
//    }
//
//    return false; // El candidato con la cédula especificada no está en la lista de candidatos de ningún partido
//}

}
