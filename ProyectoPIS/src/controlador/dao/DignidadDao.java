/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.dao;

import controlador.ed.lista.ListaEnlazada;
import controlador.exception.PosicionException;
import controlador.exception.VacioException;
import java.io.IOException;
import javax.swing.JOptionPane;
import modelo.Candidato;
import modelo.Dignidad;

/**
 *
 * @author mt3k
 */
public class DignidadDao extends AdaptadorDAO<Dignidad> {

    private Dignidad dignidad;

    public DignidadDao() {
        super(Dignidad.class);
    }

//    public void guardar() throws IOException {
//        try {
//            if (buscarPorNombre(dignidad.getNombre()) == null) {
//                dignidad.setId(generarId());
//                this.guardar(dignidad);
//            } else {
//                JOptionPane.showMessageDialog(null, "YA ESTA REGISTRADA ESTA DIGNIDAD","ERROR", JOptionPane.ERROR_MESSAGE);
//            }
//        } catch (Exception e) {
//        }
//    }
    
     public Integer guardar() throws Exception {
        return this.guardar(dignidad);

    }

    public boolean modificar() throws Exception{
            this.modificar(dignidad);
            return true;
        

    }
    

/**
 * Busca una dignidad por su nombre en la lista de dignidades.
 *
 * @param dato El nombre de la dignidad que se desea buscar.
 * @return La dignidad encontrada con el nombre especificado, o null si no se encuentra.
 * @throws Exception Si ocurre algún error durante el proceso de búsqueda.
 */
    public Dignidad buscarPorNombre(String dato) throws Exception {
        Dignidad resultado = null;
        ListaEnlazada<Dignidad> lista = listar();
        for (int i = 0; i < lista.size(); i++) {
            Dignidad aux = lista.get(i);
            if (aux.getNombre().toLowerCase().equals(dato.toLowerCase())) {
                resultado = aux;
                break;
            }
        }
        return resultado;
    }

  

    private Integer generateID() {
        return listar().size() + 1;
    }

    public Dignidad getDignidad() {
        if (this.dignidad == null) {
            this.dignidad = new Dignidad();
        }
        return dignidad;
    }

    public void setDignidad(Dignidad dignidad) {
        this.dignidad = dignidad;
    }

    /**
 * Ordena una lista enlazada de objetos de tipo Dignidad alfabéticamente por nombre.
 *
 * @param lista La lista enlazada de Dignidad que se desea ordenar.
 * @return Una instancia de ListaEnlazada con los objetos Dignidad ordenados por nombre.
 */
    public ListaEnlazada<Dignidad> ordenarDignidad(ListaEnlazada<Dignidad> lista) {
        try {
            Dignidad[] matriz = lista.toArray();
            for (int i = 1; i < lista.size(); i++) {
                Dignidad key = lista.get(i);
                int j = i - 1;
                while (j >= 0 && (matriz[j].getNombre().compareToIgnoreCase(key.getNombre())) > 0) {
                    //lista.update(j+1, lista.get(j));
                    matriz[j + 1] = matriz[j];
                    j = j - 1;
                }
                //lista.update(j+1, key);
                matriz[j + 1] = key;
            }
            lista.toList(matriz);
        } catch (Exception e) {
        }
        return lista;
    }

   
/**
 * Busca y recupera un objeto de tipo Dignidad por su ID.
 *
 * @param id El ID de la Dignidad que se desea buscar.
 * @return Un objeto de tipo Dignidad que coincide con el ID proporcionado, o null si no se encuentra.
 * @throws Exception Si ocurre algún problema durante la búsqueda y recuperación de la Dignidad.
 */

    public Dignidad buscarPorId(Integer id) throws Exception {
        Dignidad resultado = null;
        ListaEnlazada<Dignidad> lista = listar();
        for (int i = 0; i < lista.size(); i++) {
            Dignidad aux = lista.get(i);
            if (aux.getId().equals(id)) {
                resultado = aux;
                break;
            }
        }
        return resultado;
    }


    
    
}
