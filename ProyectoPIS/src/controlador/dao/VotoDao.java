package controlador.dao;

import controlador.ed.lista.ListaEnlazada;
import controlador.ed.lista.exception.PosicionException;
import controlador.exception.VacioException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import modelo.Dignidad;
import modelo.Eleccion;

import modelo.TipoVoto;
import modelo.Voto;

/**
 *
 * @author FA506ICB-HN114W
 */
public class VotoDao extends AdaptadorDAO<Voto> {

    private Voto voto;
    //variables para el cconteo de votos
    private int contadorVotosValidos = 0;
    private int contadorVotosBlancos = 0;
    private int contadorVotosNulos = 0;

    public VotoDao() {
        super(Voto.class);
    }

    public Voto getVoto() {
        if (this.voto == null) {
            this.voto = new Voto();
        }
        return voto;
    }

    public void setVoto(Voto voto) {
        this.voto = voto;
    }

    public Integer guardar() throws Exception {
        return this.guardar(voto);

    }

    public boolean modificar() throws Exception {
        this.modificar(voto);
        return true;

    }

    public Integer generateID() {
        return listar().size() + 1;
    }

    public int getContadorVotosValidos() {
        return contadorVotosValidos;
    }

    public void setContadorVotosValidos(int contadorVotosValidos) {
        this.contadorVotosValidos = contadorVotosValidos;
    }

    public int getContadorVotosBlancos() {
        return contadorVotosBlancos;
    }

    public void setContadorVotosBlancos(int contadorVotosBlancos) {
        this.contadorVotosBlancos = contadorVotosBlancos;
    }

    public int getContadorVotosNulos() {
        return contadorVotosNulos;
    }

    public void setContadorVotosNulos(int contadorVotosNulos) {
        this.contadorVotosNulos = contadorVotosNulos;
    }

 /**
 * Registra los votos de una lista en la base de datos.
 *
 * @param listaVotoGuardar  Lista enlazada de votos a ser registrados.
 * @throws VacioException    Excepción lanzada si la lista de votos está vacía.
 * @throws PosicionException Excepción lanzada si ocurre un error en la posición de la lista.
 * @throws Exception        Excepción general en caso de algún error durante el proceso de votación.
 */
public void votar(ListaEnlazada<Voto> listaVotoGuardar) throws VacioException, PosicionException,  Exception {
    try {
        // Iterar a través de la lista de votos
        for (int i = 0; i < listaVotoGuardar.size(); i++) {
            Voto voto = listaVotoGuardar.get(i);
            
            // Asignar un nuevo ID al voto antes de guardarlo (si es necesario)
            voto.setId(generateID());
            
            // Guardar cada voto en la base de datos
            guardar(voto);
        }
    } catch (IOException e) {
        e.printStackTrace();
        // Lógica adicional de manejo de excepciones, si es necesario.
    }
}
    
   /**
 * Genera una lista enlazada de votos a partir de un mapa de votos y una elección.
 *
 * @param mapa      Mapa que contiene el ID de la dignidad como clave y el voto como valor.
 * @param eleccion  La elección en la que se registran los votos.
 * @return          Una lista enlazada de votos generada a partir del mapa y la elección.
 */
public ListaEnlazada<Voto> listaVotoGuardar(HashMap<Integer, String> mapa, Eleccion eleccion) {
    ListaEnlazada<Voto> lista = new ListaEnlazada<>();
    int idEleccion = eleccion.getId(); // Obtener el ID de la elección

    // Iterar a través del mapa de votos
    for (Map.Entry<Integer, String> set : mapa.entrySet()) {
        // Obtener la dignidad correspondiente al ID de la clave en el mapa
        Dignidad d = new DignidadDao().obtener(set.getKey());
        Voto v = new Voto();

        // Establecer las propiedades del voto
        v.setId_dignidad(d.getId());
        v.setVoto(set.getValue());
        v.setId_eleccion(idEleccion); //se modifica de forma dinamica

        // Verificar si el voto es blanco, válido o nulo
        if (set.getValue().equalsIgnoreCase("")) {
            v.setTipo(TipoVoto.BLANCO);
            contadorVotosBlancos++;
        } else {
            String aux[] = set.getValue().split(";");
            if (aux.length <= d.getNro()) {
                v.setTipo(TipoVoto.VALIDO);
                contadorVotosValidos++;
            } else {
                v.setTipo(TipoVoto.NULO);
                contadorVotosNulos++;
            }
        }
        
        // Insertar el voto en la lista enlazada
        lista.insertar(v);
    }
    
    return lista;
}

    
    
    public int getContadorVotosPorCandidato(int idCandidato) {
        // Aquí deberías realizar la consulta a tu base de datos para obtener el contador de votos por candidato
        // Puedes utilizar JDBC u otra biblioteca de acceso a base de datos

        int contador = 0; // Valor de ejemplo, debes reemplazarlo con la lógica real de tu consulta

        return contador;
    }

}
