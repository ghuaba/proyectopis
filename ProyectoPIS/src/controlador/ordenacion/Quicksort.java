/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.ordenacion;

import controlador.ed.lista.ListaEnlazada;
import controlador.exception.PosicionException;
import controlador.exception.VacioException;
import modelo.Eleccion;
import modelo.Papeleta;
import modelo.Persona;

/**
 *
 * @author mt3k
 */
public class Quicksort<E> {

    public ListaEnlazada<Persona> quicksortCedulaCandidato(ListaEnlazada<Persona> lista, boolean ascendente) throws VacioException, PosicionException, controlador.ed.lista.exception.VacioException, controlador.ed.lista.exception.PosicionException {
        quicksortRecursivoCedulaCandidato(lista, 0, lista.getSize() - 1, ascendente);
        return lista;
    }

    private void quicksortRecursivoCedulaCandidato(ListaEnlazada<Persona> lista, int inicio, int fin, boolean ascendente) throws VacioException, PosicionException, controlador.ed.lista.exception.VacioException, controlador.ed.lista.exception.PosicionException {
        if (inicio < fin) {
            int indicePivote = particionCedulaCandidato(lista, inicio, fin, ascendente);
            quicksortRecursivoCedulaCandidato(lista, inicio, indicePivote - 1, ascendente);
            quicksortRecursivoCedulaCandidato(lista, indicePivote + 1, fin, ascendente);
        }
    }

    private int particionCedulaCandidato(ListaEnlazada<Persona> lista, int inicio, int fin, boolean ascendente) throws VacioException, PosicionException, controlador.ed.lista.exception.VacioException, controlador.ed.lista.exception.PosicionException {
        Persona pivote = lista.get(fin);
        int i = inicio - 1;

        for (int j = inicio; j < fin; j++) {
            Persona elementoActual = lista.get(j);
            if ((ascendente && elementoActual.getCedula().compareToIgnoreCase(pivote.getCedula()) <= 0)
                    || (!ascendente && elementoActual.getCedula().compareToIgnoreCase(pivote.getCedula()) >= 0)) {
                i++;
                lista.intercambioElementos(i, j);
            }
        }

        lista.intercambioElementos(i + 1, fin);
        return i + 1;
    }
    
    
    
    
   public ListaEnlazada<Eleccion> quicksortIdentificadorEleccion(ListaEnlazada<Eleccion> lista, boolean ascendente) throws VacioException, PosicionException, controlador.ed.lista.exception.VacioException, controlador.ed.lista.exception.PosicionException {
    quicksortRecursivoIdentificadorEleccion(lista, 0, lista.getSize() - 1, ascendente);
    return lista;
}

private void quicksortRecursivoIdentificadorEleccion(ListaEnlazada<Eleccion> lista, int inicio, int fin, boolean ascendente) throws VacioException, PosicionException, controlador.ed.lista.exception.VacioException, controlador.ed.lista.exception.PosicionException {
    if (inicio < fin) {
        int indicePivote = particionIdentificadorEleccion(lista, inicio, fin, ascendente);
        quicksortRecursivoIdentificadorEleccion(lista, inicio, indicePivote - 1, ascendente);
        quicksortRecursivoIdentificadorEleccion(lista, indicePivote + 1, fin, ascendente);
    }
}

private int particionIdentificadorEleccion(ListaEnlazada<Eleccion> lista, int inicio, int fin, boolean ascendente) throws VacioException, PosicionException, controlador.ed.lista.exception.VacioException, controlador.ed.lista.exception.PosicionException {
    Eleccion pivote = lista.get(fin);
    int i = inicio - 1;

    for (int j = inicio; j < fin; j++) {
        Eleccion elementoActual = lista.get(j);
        int comparacion = Integer.compare(elementoActual.getNro_identificador(), pivote.getNro_identificador());

        if ((ascendente && comparacion <= 0) || (!ascendente && comparacion >= 0)) {
            i++;
            lista.intercambioElementos(i, j);
        }
    }

    lista.intercambioElementos(i + 1, fin);
    return i + 1;
}







public ListaEnlazada<Papeleta> quicksortNumeroPapeleta(ListaEnlazada<Papeleta> lista, boolean ascendente) throws VacioException, PosicionException, controlador.ed.lista.exception.VacioException, controlador.ed.lista.exception.PosicionException {
    quicksortRecursivoNumeroPapeleta(lista, 0, lista.getSize() - 1, ascendente);
    return lista;
}

private void quicksortRecursivoNumeroPapeleta(ListaEnlazada<Papeleta> lista, int inicio, int fin, boolean ascendente) throws VacioException, PosicionException, controlador.ed.lista.exception.VacioException, controlador.ed.lista.exception.PosicionException {
    if (inicio < fin) {
        int indicePivote = particionNumeroPapeleta(lista, inicio, fin, ascendente);
        quicksortRecursivoNumeroPapeleta(lista, inicio, indicePivote - 1, ascendente);
        quicksortRecursivoNumeroPapeleta(lista, indicePivote + 1, fin, ascendente);
    }
}

private int particionNumeroPapeleta(ListaEnlazada<Papeleta> lista, int inicio, int fin, boolean ascendente) throws VacioException, PosicionException, controlador.ed.lista.exception.VacioException, controlador.ed.lista.exception.PosicionException {
    Papeleta pivote = lista.get(fin);
    int i = inicio - 1;

    for (int j = inicio; j < fin; j++) {
        Papeleta elementoActual = lista.get(j);
        int comparacion = Integer.compare(elementoActual.getNro(), pivote.getNro());

        if ((ascendente && comparacion <= 0) || (!ascendente && comparacion >= 0)) {
            i++;
            lista.intercambioElementos(i, j);
        }
    }

    lista.intercambioElementos(i + 1, fin);
    return i + 1;
}


}
