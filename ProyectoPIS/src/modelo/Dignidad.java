package modelo;

/**
 *
 * @author mt3k
 */
public class Dignidad {

    private Integer id;
    private Integer nro;
    private String nombre;
    private Boolean estado;  //era de tipo byte

    public Integer getNro() {
        return nro;
    }

    public void setNro(Integer nro) {
        this.nro = nro;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

//    public byte getEstado() {
//        return estado;
//    }
//
//    public void setEstado(byte estado) {
//        this.estado = estado;
//    }
//
//    public boolean isActivo() {
//        return estado == 1;
//    }

    public Boolean getEstado() {
        return estado ;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }
    
    

}
