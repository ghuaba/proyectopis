package modelo;

import controlador.dao.CandidatoDao;
import controlador.dao.DignidadDao;
import controlador.dao.PartidoPoliticoDao;
import controlador.dao.PersonaDAO;
import controlador.ed.lista.ListaEnlazada;
import controlador.ed.lista.exception.PosicionException;
import controlador.exception.VacioException;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;

/**
 *
 * @author FA506ICB-HN114W
 */
public class PapeletaComponent {

    //key llelva = id_dignidad
    private HashMap<Integer, String> mapa = new HashMap();

    public HashMap getMap() {
        return mapa;
    }

    /**
     * Construye las pestañas de la papeleta en un objeto JTabbedPane.
     *
     * @param tabbed El objeto JTabbedPane donde se agregarán las pestañas.
     * @param dd Instancia de DignidadDao para obtener información sobre las
     * dignidades.
     * @param cd Instancia de CandidatoDao para obtener información sobre los
     * candidatos.
     * @throws VacioException Si se encuentra una lista vacía en algún punto del
     * proceso.
     * @throws PosicionException Si se produce un error al acceder a una
     * posición en la lista.
     * @throws Exception Si ocurre algún otro error durante el proceso de
     * construcción.
     */
    public void construirPapeleta(JTabbedPane tabbed, DignidadDao dd, CandidatoDao cd) throws VacioException, PosicionException, Exception {

        System.out.println("Comienza la construcción de la papeleta");
        ListaEnlazada<Dignidad> lista = dd.listar();

        for (int i = 0; i < lista.size(); i++) {
            Dignidad d = lista.get(i);
            if (d.getEstado()) {  // Comprueba si la dignidad está activa
                ListaEnlazada<Partido> listaP = new PartidoPoliticoDao().listaPartidosDignidad(d.getId());

                if (!listaP.estaVacia()) {
                    mapa.put(d.getId(), "");

                    // Creación del panel principal de la dignidad
                    JPanel panePrincipal = new JPanel();
                    panePrincipal.setBackground(new Color(240, 240, 240));
                    panePrincipal.setBorder(BorderFactory.createEtchedBorder(1, new Color(173, 216, 230), Color.DARK_GRAY));
                    panePrincipal.setPreferredSize(new Dimension(500, 400));

                    for (int k = 0; k < listaP.size(); k++) {
                        Partido partidoPolitico = listaP.get(k);

                        // Creación de un panel para cada partido político
                        JPanel pane = new JPanel();
                        pane.setBackground(new Color(255, 255, 255));
                        pane.setBorder(BorderFactory.createEtchedBorder(1, new Color(211, 211, 211), Color.DARK_GRAY));
                        pane.setPreferredSize(new Dimension(400, 700));

                        GridBagLayout grid = new GridBagLayout();
                        GridBagConstraints c = new GridBagConstraints();
                        pane.setLayout(grid);

                        c.gridx = 0;
                        c.gridy = 0;

                        JLabel lblPartido = new JLabel(partidoPolitico.getNombre());
                        pane.add(lblPartido, c);

                        // Agrega candidatos al panel
                        ListaEnlazada<Candidato> listaC = cd.buscarPorDignidadPartido(d.getId(), partidoPolitico.getId());
                        if (!listaC.estaVacia()) {
                            for (int j = 0; j < listaC.size(); j++) {
                                c.gridy = j + 1;
                                Candidato aux = listaC.get(j);
                                if (aux.getTipo().toString().equalsIgnoreCase(TipoC.PRINCIPAL.toString())) {
                                    pane.add(candidatos(aux), c);
                                }
                            }
                        }

                        // Agrega un panel de desplazamiento al panel de partido
                        JScrollPane scrollPane = new JScrollPane(pane);
                        scrollPane.setBorder(BorderFactory.createLineBorder(new Color(255, 182, 193)));
                        scrollPane.setBackground(new Color(240, 240, 240));
                        scrollPane.setPreferredSize(new Dimension(400, 300));
                        scrollPane.setSize(new Dimension(400, 300));
                        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
                        scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

                        panePrincipal.add(scrollPane);
                    }

                    // Agrega la pestaña al objeto JTabbedPane
                    tabbed.addTab(d.getNombre(), panePrincipal);
                }
            }
        }
    }

    /**
     * Crea un panel que representa un candidato en la papeleta.
     *
     * @param candidato El candidato que se representará en el panel.
     * @return Un JPanel que contiene la información y opciones del candidato.
     */
    private JPanel candidatos(Candidato candidato) {
        GridBagLayout grid = new GridBagLayout();
        GridBagConstraints c = new GridBagConstraints();
        JPanel panel = new JPanel();
//        panel.setBorder(BorderFactory.createEtchedBorder(1)); //este es el original
        panel.setBorder(BorderFactory.createEtchedBorder(1, Color.GREEN, Color.DARK_GRAY)); // Agrega colores al borde
        panel.setLayout(grid);
        panel.setPreferredSize(new Dimension(300, 700));
        panel.setSize(new Dimension(300, 700));
        c.insets = new Insets(10, 10, 10, 10);
        c.gridwidth = 3;
        c.gridx = 0;
        c.gridy = 0;

        //para cargar las fotos
//        JLabel lblfoto = new JLabel();
//        //lblfoto.setIcon(scaleImage(new ImageIcon(getClass().getResource("/vista/candidato/" + candidato.getFoto())), 200, 150));
//        lblfoto.setSize(new Dimension(200, 150));
//        lblfoto.setPreferredSize(new Dimension(200, 150));
//        panel.add(lblfoto, c);
        c.gridx = 1;
        c.gridy = 1;
        JLabel lblNombre = new JLabel(new PersonaDAO().obtener(candidato.getId()).toString());
        panel.add(lblNombre, c);
        c.gridx = 1;
        c.gridy = 2;
        JCheckBox checkBox = new JCheckBox("VOTAR");
        checkBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                JCheckBox aux = (JCheckBox) ae.getSource();
                String valor = mapa.get(candidato.getId_dignidad()).toString();
                String voto = "candidato" + ":" + candidato.getId() + "," + "dignidad" + ":" + candidato.getId_dignidad() + "," + "partido" + ":" + candidato.getId_partido();
                if (aux.isSelected()) {
                    if (valor.isEmpty()) {
                        valor = voto;
                    } else {
                        valor += ";" + voto;
                    }
                    mapa.put(candidato.getId_dignidad(), valor);
                } else {
                    String[] trozar = valor.split(";");
                    if (trozar.length == 1) {
                        valor = "";
                    } else {
                        valor = "";
                        for (String vot : trozar) {
                            if (!vot.equalsIgnoreCase(voto)) {
                                valor += vot + ";";
                            }
                        }
                        valor = valor.substring(0, valor.length() - 1);
                    }
                    mapa.put(candidato.getId_dignidad(), valor);
                }
            }

        });
        panel.add(checkBox, c);
        return panel;
    }

    /**
     * Escala una imagen representada por un ImageIcon al tamaño deseado.
     *
     * @param icon El ImageIcon de la imagen a escalar.
     * @param w El ancho deseado para la imagen escalada.
     * @param h El alto deseado para la imagen escalada.
     * @return Un nuevo ImageIcon que representa la imagen escalada.
     */
    public ImageIcon scaleImage(ImageIcon icon, int w, int h) {
        int nw = icon.getIconWidth();
        int nh = icon.getIconHeight();

        // Escala la imagen si su ancho es mayor que el ancho deseado
        if (icon.getIconWidth() > w) {
            nw = w;
            nh = (nw * icon.getIconHeight()) / icon.getIconWidth();
        }
        // Escala la imagen si su alto es mayor que el alto deseado
        if (nh > h) {
            nh = h;
            nw = (icon.getIconWidth() * nh) / icon.getIconHeight();
        }

        // Escala la imagen y crea un nuevo ImageIcon escalado
        return new ImageIcon(icon.getImage().getScaledInstance(nw, nh, Image.SCALE_DEFAULT));
    }

}
