/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo;

/**
 *
 * @author ghuaba
 */
public class Persona {
    
    private Integer id;
    private String nombres;
    private String apellidos;
    private String cedula;
    private String sexo;
    private Integer edad;
    
    private String correo;
    private Boolean estado;
    private String contrasena;
    private Integer id_papeleta;
    
     //info de login pero inicias secion con cedula

//    private Integer id_eleccion;  // se considera que mejor tenga la realcion con el voto 

    
     public Persona() {
    }

    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

   
    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }



    public Integer getEdad() {
        return edad;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public Integer getId_papeleta() {
        return id_papeleta;
    }

    public void setId_papeleta(Integer id_papeleta) {
        this.id_papeleta = id_papeleta;
    }
    
    
    

//    public Integer getId_eleccion() {
//        return id_eleccion;
//    }
//
//    public void setId_eleccion(Integer id_eleccion) {
//        this.id_eleccion = id_eleccion;
//    }
    
    
    
     @Override
    public String toString() {
//        return id + " " + nombres + " " + apellidos + " " + correo + " " + cedula + " " + contrasena;
    return nombres + " " + apellidos;
    }
    
//     public String desencriptarContrasena(String contrasena) {
//        try {
//            MessageDigest digest = MessageDigest.getInstance("SHA-256");
//            byte[] hash = digest.digest(contrasena.getBytes());
//            StringBuilder hexString = new StringBuilder();
//            for (byte b : hash) {
//                String hex = Integer.toHexString(0xff & b);
//                if (hex.length() == 1) {
//                    hexString.append('0');
//                }
//                hexString.append(hex);
//            }
//            return hexString.toString();
//        } catch (NoSuchAlgorithmException e) {
//            // Manejo de error si el algoritmo no es compatible
//            e.printStackTrace();
//            return null;
//        }
//    }

   
}