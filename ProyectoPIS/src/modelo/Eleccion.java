/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo;

import java.util.Date;

/**
 *
 * @author ghuaba
 */
public class Eleccion {

    private Integer id;
    private Boolean estado; //true-false
    private Date fecha_ini;
    private Date fecha_fin;
    private Integer nro_identificador;
 

  

    public Eleccion() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public Date getFecha_ini() {
        return fecha_ini;
    }

    public void setFecha_ini(Date fecha_ini) {
        this.fecha_ini = fecha_ini;
    }

    public Date getFecha_fin() {
        return fecha_fin;
    }

    public void setFecha_fin(Date fecha_fin) {
        this.fecha_fin = fecha_fin;
    }

    public Integer getNro_identificador() {
        return nro_identificador;
    }

    public void setNro_identificador(Integer nro_identificador) {
        this.nro_identificador = nro_identificador;
    }

    
    
}
