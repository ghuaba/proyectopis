package modelo;

/**
 *
 * @author mt3k
 */
//public class Candidato extends Persona {
public class Candidato {

    private Integer id;
    private Integer id_dignidad;
    private Integer id_partido;
    private TipoC tipo;
    private String foto;
    private Integer id_persona;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoC getTipo() {
        return tipo;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public void setTipo(TipoC tipo) {
        this.tipo = tipo;
    }

    public Integer getId_dignidad() {
        return id_dignidad;
    }

    public void setId_dignidad(Integer id_dignidad) {
        this.id_dignidad = id_dignidad;
    }

    public Integer getId_partido() {
        return id_partido;
    }

    public void setId_partido(Integer id_partido) {
        this.id_partido = id_partido;
    }

    public Integer getId_persona() {
        return id_persona;
    }

    public void setId_persona(Integer id_persona) {
        this.id_persona = id_persona;
    }

}
