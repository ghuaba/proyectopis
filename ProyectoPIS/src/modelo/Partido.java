/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo;


/**
 *
 * @author ghuaba
 */
public class Partido {

    private Integer id;
    private String nombre;
    private String eslogan;
    private String logo;
    private String nroLista;
    private String siglas;
    
    //    private ListaEnlazada<Candidato> candidatos;  //el inge tampoco tiene una lista de candidatos

    
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getNroLista() {
        return nroLista;
    }

    public void setNroLista(String nroLista) {
        this.nroLista = nroLista;
    }

    public String getSiglas() {
        return siglas;
    }

    public void setSiglas(String siglas) {
        this.siglas = siglas;
    }
    
    
    
//
//    public ListaEnlazada<Candidato> getCandidatos() {
//        if (candidatos == null) {
//            candidatos = new ListaEnlazada<>();
//        }
//
//        return candidatos;
//    }
//
//    public void setCandidatos(ListaEnlazada<Candidato> candidatos) {
//        this.candidatos = candidatos;
//    }

//    public void agregarCandidato(Candidato candidato, Integer dignidad_id) throws controlador.ed.lista.exception.VacioException, controlador.ed.lista.exception.PosicionException {
//        boolean dignidadRepetida = false;
//        ListaEnlazada<Candidato> candidatos = getCandidatos();
//
//        for (int i = 0; i < candidatos.size(); i++) {
//            Candidato c = candidatos.get(i);
//            if (c.getId_dignidad() != null && c.getId_dignidad().equals(dignidad_id)) {
//                dignidadRepetida = true;
//                break;
//            }
//        }
//
//        if (dignidadRepetida) {
//            JOptionPane.showMessageDialog(null, "Ya hay un candidato con la misma dignidad en este partido", "ERROR", JOptionPane.ERROR_MESSAGE);
//        } else {
//            candidato.setId_dignidad(dignidad_id);
//            candidatos.insertar(candidato);
//        }
//    }

    public String getEslogan() {
        return eslogan;
    }

    public void setEslogan(String eslogan) {
        this.eslogan = eslogan;
    }

}
