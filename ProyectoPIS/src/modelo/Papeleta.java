/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo;

import java.util.Date;

/**
 *
 * @author ghuaba
 */


//usar esta clase para verificar que dicha persona guardo el voto en dicha fecha
public class Papeleta {
    
    private Integer id;
    private Date fecha;
    private Integer nro;
//    private Date hora; este si debe ir de ley porque la fechas no se guaurdan completas en la base sino solo la fecha o solo la hora  
    
    
//    private Integer id_candidato;
//    private Integer nroPapeleta;
    
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

//    public Integer getNroPapeleta() {
//        return nroPapeleta;
//    }
//
//    public void setNroPapeleta(Integer nroPapeleta) {
//        this.nroPapeleta = nroPapeleta;
//    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }


    
//
//    public Integer getId_persona() {
//        return id_persona;
//    }
//
//    public void setId_persona(Integer id_persona) {
//        this.id_persona = id_persona;
//    }

    public Integer getNro() {
        return nro;
    }

    public void setNro(Integer nro) {
        this.nro = nro;
    }



   
    
    
    
}
